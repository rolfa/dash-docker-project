import psycopg2
import psycopg2.extras as extras
from psycopg2.sql import Identifier, SQL
from typing import (
    Tuple,
    Any,
    Optional,
    Union,
    List,
    NoReturn
)
from psycopg2._psycopg import connection as psycopg2Connection
from psycopg2.sql import Composed
import pandas as pd
from datetime import datetime

from utils.functions import (
    get_params_from_json_file
)

from config.config import (
    PATH_TO_CREDENTIALS,
    SERVER_TO_USE
)



def run_query(
        conn: psycopg2Connection,
        query: Union[str, Composed],
        vars: Tuple[Any,...] = None,
        return_data: bool = False,
        return_rowscount: bool = False,
        return_df: bool = False
) -> Optional[pd.DataFrame]:

    cur = conn.cursor()

    try:
        cur.execute(query, vars=vars)
        conn.commit()
        if return_data: return cur.fetchall()
        elif return_rowscount: return cur.rowcount
        elif return_df:
            data = cur.fetchall()
            headers = cur.description
            headers = [headers[k][0] for k in range(len(headers))]
            return pd.DataFrame(data=data, columns=headers)

    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cur.close()
        return 1

    cur.close()



#   https://naysan.ca/2020/05/09/pandas-to-postgresql-using-psycopg2-bulk-insert-performance-benchmark/
def upload_df_to_table(conn: psycopg2Connection, df: pd.DataFrame, schema: str, table: str) -> None:
    # make sure we have None instead of a string to handle NULLs in SQL correctly
    clean_list = df.to_numpy().tolist()
    for i, item in enumerate(clean_list):
        clean_list[i] = [None if pd.isnull(x) else x for x in item]

    # prepare tuples for easy use of INSERT
    tuples = [tuple(x) for x in clean_list]
    cols = list(df.columns)
    query = SQL("INSERT INTO {tab_var} ({col_var}) VALUES %s").format(
        tab_var=Identifier(schema, table),
        col_var=SQL(', ').join(map(Identifier, cols))
    )
    cursor = conn.cursor()

    try:
        extras.execute_values(cursor, query, tuples)
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    cursor.close()



def add_entry_to_last_updated_table(conn: psycopg2Connection, table_name: str, timestamp: datetime) -> None:
    query = "INSERT INTO visible.last_updated SELECT %s, %s"
    run_query(conn=conn, query=query, vars=(table_name, timestamp))



# https://stackoverflow.com/questions/37926248/how-to-remove-accents-from-values-in-columns
def return_string_column_without_accents(df: pd.DataFrame, column: str) -> pd.DataFrame:
    return  df[column].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')



def return_df_with_string_columns_without_accents(
        df: pd.DataFrame,
        columns: Union[str, List[str]]
) -> pd.DataFrame:

    if isinstance(columns, str):
        columns = [columns]

    for col in columns:
        df[col] = return_string_column_without_accents(df, col)

    return df



def get_last_updated_value() -> str:
    conn_params = get_params_from_json_file(PATH_TO_CREDENTIALS, SERVER_TO_USE)
    conn = psycopg2.connect(**conn_params)
    query = "SELECT MAX(last_updated_timestamp) FROM visible.last_updated WHERE TABLE_NAME = 'orders';"
    last_updated_vaue = run_query(conn=conn, query=query, return_data=True)
    return last_updated_vaue[0][0].strftime('%Y-%m-%d %H:%M:%S')

import json
import pandas as pd
import numpy as np
import random
import uuid
from datetime import (
    datetime,
    timedelta
)

from typing import (
    Dict,
    Optional,
    TypedDict,
    Union,
    List,
    Any
)

from utils.names import (
    SURNAMES,
    LASTNAMES
)

from config.config import (
    DEBUG_PRINT
)



def return_df_column_or_0_if_empty(df: pd.DataFrame, col: str) -> Union[pd.DataFrame, int]:
    if not df.columns.isin([col]).any():
        raise Exception('Column does not exist in df.')
    else:
        if not df.empty:
            return pd.DataFrame(df[col])
        else:
            return 0



def get_value_by_colname_from_one_row_df_or_0_if_empty(df: pd.DataFrame, col: str) -> float:
    if len(df) > 1:
        raise Exception('df has more than one row.')
    elif not df.columns.isin([col]).any():
        raise Exception('Column does not exist in df.')
    else:
        if not df.empty:
            return df[col].item()
        else:
            return 0



def load_json_from_file(path: str) -> Dict[str, str]:
    with open(path) as f:
        return json.load(f)



def get_params_from_json_file(path: str, key: Optional[str] = None) -> Dict[str, str]:
    if key is None:
        return load_json_from_file(path)
    elif key is not None:
        return load_json_from_file(path)[key]



def get_date_for_n_days_in_the_past_as_str(n: int) -> str:
    return (datetime.now() - timedelta(days=n)).strftime("%Y-%m-%d")



def generate_df_with_random_surnames_and_lastnames(
        n: int,
        colnames: List[str] = ['surnames', 'lastnames'],
        seed: int = None
) -> pd.DataFrame:
    curRandom = random.Random(seed)
    surnames = curRandom.choices(SURNAMES, k=n)
    lastnames = curRandom.choices(LASTNAMES, k=n)
    return pd.DataFrame(data={colnames[0]: surnames, colnames[1]: lastnames})



def make_nice_table_header_from_colnames(
        columns: pd.Index,
        hide_id_column: bool = True,
        additional_params: Union[List[Dict[str, Any]], None] = None
) -> List[Dict[str, Any]]:

    if 'id' in columns and hide_id_column:
        columns = columns.drop('id')

    list_of_col_dicts = [
        {'name': s.replace('_', ' ').capitalize().replace('id', 'ID'), 'id': s} for s in columns
    ]

    if additional_params is not None:
        df_orig_params = pd.DataFrame(list_of_col_dicts)
        df_additional_params = pd.DataFrame(additional_params)
        list_of_col_dicts = df_orig_params\
            .merge(
                df_additional_params,
                on='id',
                how='left'
            )\
            .to_dict(
                orient='records'
            )

        # clean up NaN key values
        for i, col_dict in enumerate(list_of_col_dicts):
            list_of_col_dicts[i] = {k: v for k, v in col_dict.items() if v is not np.nan}

    return list_of_col_dicts



def capitalize_column_but_keep_fillwords(
        df: pd.DataFrame,
        column: str,
        fillwords: Union[List[str], None] = [
            'di',
            'do',
            'dos',
            'de',
            'del',
            'da',
            'le',
            'la',
            'los'
        ]
) -> pd.DataFrame:
    df[column] = df[column].str.title()
    if not fillwords is  None:
        fillwords_dict = {' ' + s.capitalize() + ' ' : ' ' + s.lower() + ' ' for s in fillwords}
        df[column] = df[column].replace(fillwords_dict, regex=True)

    return df



def generate_unique_identifier() -> str:
    return str(uuid.uuid4())




def is_json(data) -> bool:
    try:
        json_object = json.loads(data)
    except Exception as e:
        return False
    return True



def is_DataFrame(data) -> bool:
    return isinstance(data, pd.DataFrame)



def convert_json_to_df(data: TypedDict) -> pd.DataFrame:
    return pd.read_json(data, orient='split')



def debug_print(msg:str, prefix: str = '[APP DEBUG]', timestamp: bool = True):
    if DEBUG_PRINT:
        _ts = ''
        if timestamp:
            _ts = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        print(f'{prefix} {_ts} -- {msg}')

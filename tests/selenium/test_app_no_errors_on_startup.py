from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

from config.config import (
    PORT,
)



# This setup is only working in combination with docker-compose files for testing
def test_no_errors_at_startup_and_after_clicking_in_oders_table(start_app, run_update_visible):
    """
    GIVEN startup of the app, nothing has been clicked yet
    WHEN wait until everything has loaded
    THEN no errors should occur in dash devtools error log
    """

    firefox_options = webdriver.FirefoxOptions()
    driver = webdriver.Remote(
        command_executor='http://firefox:4444',
        options=firefox_options
    )
    URL = f'http://app:{PORT}'
    driver.get(URL)


    # first, wait for orders table to get loaded and updated
    # wait until updated (i.e. filled with data) by checking if a row exists
    try:
        css_selector = "#table_orders tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2)"
        element = WebDriverWait(driver, timeout=30).until(EC.presence_of_element_located(
            (By.CSS_SELECTOR, css_selector)
        ))
        assert element.is_displayed, "check that row content of table_orders is displayed"
    except TimeoutException:
        print("Loading took too much time!")


    # click an element in table (id column) to also check if that will cause an error
    css_selector = "#table_orders tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2)"
    element = driver.find_element(By.CSS_SELECTOR, css_selector)
    element.click()


    # click the debug icon and check if the Errors icon shows errors (via its label)
    css_selector = "#_dash-global-error-container > div > div.dash-debug-menu.dash-debug-menu--closed"
    element = driver.find_element(By.CSS_SELECTOR, css_selector)
    element.click()

    css_selector = "#_dash-global-error-container > div > div.dash-debug-menu__outer.dash-debug-menu__outer--open > div > div:nth-child(2) > div > label"
    element = driver.find_element(By.CSS_SELECTOR, css_selector)
    assert element.text == "0 Errors", "devtools should not raise an error alert"

    driver.close()

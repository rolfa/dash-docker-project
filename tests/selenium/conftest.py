import pytest
from sql.update_visible import update_visible
import threading
from app import server

# need to import app and callbacks to make them available during test
from app import app
from pages.daily.daily_callbacks import (
    make_bar_1w_chart,
    make_line_18m_chart,
    init_orders_table_container_at_startup,
    make_customer_or_order_summary_table
)

from config.config import (
    PORT,
    DEBUG
)





@pytest.fixture(scope='module')
def run_update_visible():
    update_visible()





@pytest.fixture(scope='module')
def start_app():
    # activate the dash-debug-alert
    app.enable_dev_tools(debug=True)
    server_cofig = dict(
        host='0.0.0.0',
        port=PORT,
        debug=DEBUG,
        use_reloader=False
    )
    _thread = threading.Thread(target=lambda: server.run(**server_cofig))
    _thread.daemon = True
    with server.test_request_context() as ctx:
        yield _thread.start()

    import requests
    requests.get(f'http://localhost:{PORT}/shutdown')

import pytest


def test_total_orders_by_customer_not_zero(fixture_total_orders_by_customer):
    assert not 0 in fixture_total_orders_by_customer.total_orders.to_numpy()

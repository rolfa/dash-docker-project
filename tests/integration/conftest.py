import pytest
from sql.update_visible import update_visible
from pages.daily.daily_data import (
    fetch_past_data_from_db,
    fetch_item_past_data_from_db,
    fetch_today_data_from_db,
    fetch_item_today_data_from_db,
    append_todays_data_to_past_data,
    total_orders_by_customer,
)


@pytest.fixture(scope='module')
def run_update_visible():
    update_visible()



@pytest.fixture(scope='module')
def fetch_past_data(run_update_visible):
    return fetch_past_data_from_db()



@pytest.fixture(scope='module')
def fetch_item_past_data(run_update_visible):
    return fetch_item_past_data_from_db()



@pytest.fixture(scope='module')
def fetch_today_data(run_update_visible):
    return fetch_today_data_from_db()



@pytest.fixture(scope='module')
def fetch_item_today_data(run_update_visible):
    return fetch_item_today_data_from_db()



@pytest.fixture(scope='module')
def fetch_all_data(fetch_past_data, fetch_today_data):
    return append_todays_data_to_past_data(fetch_past_data,fetch_today_data)



@pytest.fixture(scope='module')
def fetch_item_all_data(fetch_item_past_data, fetch_item_today_data):
    return append_todays_data_to_past_data(fetch_item_past_data,fetch_item_today_data)



@pytest.fixture(scope='module')
def fixture_total_orders_by_customer(fetch_all_data):
    return total_orders_by_customer(fetch_all_data)

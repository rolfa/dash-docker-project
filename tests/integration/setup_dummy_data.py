import pandas as pd
from datetime import datetime
import numpy as np
import random
import hashlib

from utils.names import (
    SURNAMES,
    LASTNAMES
)

from typing import (
    List
)


# parameters
N = 100 # size of dummy dataset
SEED = 42




def generate_random_ids(n: int = 10, seed: int = None) -> List[str]:
    curRandom = random.Random(seed)
    return [str(hashlib.md5(str(curRandom.random()).encode()).hexdigest())[:10] for _ in range(n)]



# https://stackoverflow.com/questions/50165501/generate-random-list-of-timestamps-in-python
def generate_random_timestamps(
        start: datetime = datetime.today().replace(hour=0, minute=0, second=0),
        end: datetime = datetime.now(),
        n: int = 10,
        format: str = '%H:%M:%S',
        seed: int = None
) -> List[str]:
    curRandom = random.Random(seed)
    dates = [(curRandom.random() * (end - start) + start).strftime(format) for _ in range(n)]
    return sorted(dates)



def generate_random_surnames(n: int = 10, seed: int = None) -> List[str]:
    curRandom = random.Random(seed)
    return curRandom.choices(SURNAMES, k=n)



def generate_random_lastnames(n: int = 10, seed: int = None) -> List[str]:
    curRandom = random.Random(seed)
    return curRandom.choices(LASTNAMES, k=n)



def generate_random_payment_value(likely_value: int = 120, n: int = 10, seed: int = None) -> List[str]:
    curRandom = np.random.default_rng(seed)
    return [round(curRandom.lognormal(0,0.5) * likely_value, 2) for _ in range(n)]

    

def generate_dummy_orders_table(n: int = N, seed: int = SEED) -> pd.DataFrame:
    return pd.DataFrame(
        data={
            'id': generate_random_ids(n=n, seed=seed),
            'time': generate_random_timestamps(n=n, seed=seed),
            'order_id': generate_random_ids(n=n, seed=seed),
            'customer_id': generate_random_ids(n=n, seed=seed+1),
            'surname': generate_random_surnames(n=n, seed=seed),
            'lastname': generate_random_lastnames(n=n, seed=seed),
            'payment_value': generate_random_payment_value(n=n, seed=seed)
        }
    )


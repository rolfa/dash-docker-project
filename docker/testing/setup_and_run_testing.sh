#!/bin/bash

echo "Decrypt data..."
echo $DATA_GPG_PW | gpg -d --batch --passphrase-fd 0 data.tar.gz.gpg > data.tar.gz
mkdir /data
tar -xf data.tar.gz --directory /

echo "Decrypt cred..."
echo $CRED_GPG_PW | gpg -d --batch --passphrase-fd 0 config/credentials.json.gpg > config/credentials.json

./wait-for-it.sh db:5432
./wait-for-it.sh firefox:4444

python3 -u -m sql.create_db_and_user
python3 -u -m sql.create_psql_tables
python3 -u -m sql.update_visible

pytest tests/selenium/test_app_no_errors_on_startup.py


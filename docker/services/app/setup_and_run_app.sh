#!/bin/bash

# prevent making scripts executable on host machine, since /code is bind as volume
if [ $IS_DEV ]; then
		/code.dev/wait-for-it.sh db:5432
		cd /code
else
		./wait-for-it.sh db:5432
fi


python3 -u -m sql.create_db_and_user
python3 -u -m sql.create_psql_tables
python3 -u -m sql.update_visible

gunicorn --bind 0.0.0.0:8050 -w 4 "index:server" --capture-output --log-level info --preload

# dash docker project
This is an example project that uses real e-commerce data to build a dashboard with plotly dash 
that runs in docker.

## In brief
- everything runs as services in docker: postgres, app, nginx, selenium
- daily dashboard refreshes and visualizes data as live data
- server-side caching is used for pre-calculations and individual sessions
- basic integration tests via selenium, Gitlab CI and docker on each commit


![gif](./dashboard.gif)


## Installation
**Note:** You need *docker* and *docker-compose* installed on your machine to run the dashboard locally.

Basically, download the data and run *docker-compose.dev.yaml*.

Here are step-by-step instructions for Linux.

#### Clone this repo to you local machine
```
mkdir dash-docker-project
git clone https://gitlab.com/rolfa/dash-docker-project .	
```

#### Download the olist data from kaggle and copy the csv files to /data
To download the data you need a kaggle account.

Download the data from here: https://www.kaggle.com/datasets/olistbr/brazilian-ecommerce/download

Extract and move the single csv files to a (new) folder named *data*. The folder should look like this:
```
dash-docker-project
└──data
   ├── olist_customers_dataset.csv
   ├── olist_geolocation_dataset.csv
   ├── olist_order_items_dataset.csv
   ├── olist_order_payments_dataset.csv
   ├── olist_order_reviews_dataset.csv
   ├── olist_orders_dataset.csv
   ├── olist_products_dataset.csv
   ├── olist_sellers_dataset.csv
   └── product_category_name_translation.csv
```

#### Run docker-compose.dev.yaml
Depending on you docker setup you might need to use sudo. From the root folder of the repo run:
```
sudo docker-compose -f docker-compose.dev.yaml up -d --build
```

Wait until everything is set up. Note that the first time all Python packages need to be installed. After successful creation of the containers you can check the progress in the logs
```
sudo docker-compose -f docker-compose.dev.yaml logs | grep dd_app
```

#### Visit the dashboard in you browser
Open you browser and navigate to: http://localhost:8059

#### Shutdown app
```
sudo docker-compose -f docker-compose.dev.yaml down
```

You can start the app again via:
```
sudo docker-compose -f docker-compose.dev.yaml up -d
```
Since everything is already setup this will only take seconds until the app is available.


## In more detail
### Simulating live data
The data is set up in two schemas *universe* and *visible*, where *universe* contains the whole data set in several tables. *visible* contains the same tables but filled with data only up to the current date and time. To update the data it is just copied from *universe* to *visible*. See folder *sql* for more details.
The real data actually lies completely in the past. Timestamps are set to future dates during database set up. The start date can be steered via config/config.yaml as well as whether the first month should be skipped since data is rare in this period.


### Daily Dashboard
The Daily Dashboard is built with plotly dash. By default the dashboard refreshes every minute. The update interval can be configured at the top right. This config as well as the today's data in general is individual for every individual user session. Past data aggregation is pre-computed and cached server-side and shared by all users.

To use the dashboard click into the map to select a city. Click into the orders table to view customer or order details in a second table. The scripts for the dashboard itself can be found in *layout* and *pages*.


### Testing
A basic (but important) test is run on each commit: Whether the app starts without errors and whether the tables are shown. This is done by using Gitlab CI with docker-in-docker to run the app and test it via selenium. see *docker-compose* as well as *.gitlab-ci.yaml* files for details.

To considerably speed up the Gitlab CI pipeline, multistage docker builds and caching in Gitlab container registry are used.



## Notes on dataset
The dataset was retrieved from kaggle (https://www.kaggle.com/datasets/olistbr/brazilian-ecommerce). In this repository, the data is encrypted and is only available during Gitlab CI testing. If you want to use it please download it from the link provided above.
Note, that names are not part of the original dataset. They are fictitious and generated randomly to make the data look nicer. Also, several cleaning and aggregation steps have been applied to end up with the final data that is used in the dashboard. See *sql* scripts for details.

import psycopg2
from datetime import datetime

import sys
sys.path.append("..")
from utils.pgfunctions import (
    run_query,
    add_entry_to_last_updated_table
)

from utils.functions import (
    debug_print,
    get_params_from_json_file
)

from config.config import (
    PATH_TO_CREDENTIALS,
    SERVER_TO_USE
)



def update_visible():

    conn_params = get_params_from_json_file(PATH_TO_CREDENTIALS, SERVER_TO_USE)
    conn = psycopg2.connect(**conn_params)

    # All updates shall refer to same 'now'
    now = datetime.now()



    # Orders
    # Insert/Update the different timestamps separately when they occur
    query = """
        LOCK TABLE visible.orders;
        INSERT INTO visible.orders
                (order_id, customer_id, order_status, order_purchase_timestamp)
        SELECT
                order_id,
                customer_id,
                order_status,
                order_purchase_timestamp
        FROM universe.orders
        WHERE TRUE
        AND order_purchase_timestamp <= %s
        AND order_purchase_timestamp >
            (SELECT max(last_updated_timestamp) FROM visible.last_updated WHERE table_name = 'orders')
        ORDER BY order_purchase_timestamp DESC
    """
    # run query and retrieve rowscount
    rowscount = run_query(conn=conn, query=query, vars=(now,), return_rowscount=True)
    # note: rowscount refers to SELECT not INSERT. So in other scenarios this method might not be reliable

    # Add a timestamp to last_updated if there have been insertions
    if rowscount != 0: add_entry_to_last_updated_table(conn=conn, table_name='orders', timestamp=now)


    # Update estimated_delivery date together with order_approved_at
    query = """
        LOCK TABLE visible.orders;
        UPDATE visible.orders
        SET (order_approved_at, order_estimated_delivery_date) =
                (orders0.order_approved_at, orders0.order_estimated_delivery_date)
        FROM universe.orders orders0
        WHERE TRUE
        AND orders0.order_approved_at <= %s
        AND orders0.order_approved_at >
            (SELECT max(last_updated_timestamp) - INTERVAL '3 months' FROM visible.last_updated WHERE table_name = 'orders')
        AND visible.orders.order_id = orders0.order_id
    """
    rowscount = run_query(conn=conn, query=query, vars=(now,), return_rowscount=True)


    # order_delivered_carrier_date
    query = """
        LOCK TABLE visible.orders;
        UPDATE visible.orders
        SET order_delivered_carrier_date = orders0.order_delivered_carrier_date
        FROM universe.orders orders0
        WHERE TRUE
        AND orders0.order_delivered_carrier_date <= %s
        AND orders0.order_delivered_carrier_date >
                (SELECT max(last_updated_timestamp) - INTERVAL '3 months' FROM visible.last_updated WHERE table_name = 'orders')
        AND visible.orders.order_id = orders0.order_id
    """
    rowscount = run_query(conn=conn, query=query, vars=(now,), return_rowscount=True)


    # order_delivered_customer_date
    query = """
        LOCK TABLE visible.orders;
        UPDATE visible.orders
        SET order_delivered_customer_date = orders0.order_delivered_customer_date
        FROM universe.orders orders0
        WHERE TRUE
        AND orders0.order_delivered_customer_date <= %s
        AND orders0.order_delivered_customer_date >
                (SELECT max(last_updated_timestamp) - INTERVAL '3 months' FROM visible.last_updated WHERE table_name = 'orders')
        AND visible.orders.order_id = orders0.order_id
    """
    rowscount = run_query(conn=conn, query=query, vars=(now,), return_rowscount=True)





    # order_payments
    query = """
        LOCK TABLE visible.order_payments;
        INSERT INTO visible.order_payments
        SELECT order_payments.* FROM universe.order_payments
        LEFT JOIN universe.orders USING(order_id)
        WHERE TRUE
        AND order_purchase_timestamp < %s
        AND order_purchase_timestamp >
            (SELECT max(last_updated_timestamp) FROM visible.last_updated WHERE table_name = 'order_payments')
        ORDER BY order_purchase_timestamp DESC
    """
    rowscount = run_query(conn=conn, query=query, vars=(now,), return_rowscount=True)
    if rowscount != 0: add_entry_to_last_updated_table(conn=conn, table_name='order_payments', timestamp=now)





    # order_items
    query = """
        LOCK TABLE visible.order_items;
        INSERT INTO visible.order_items
        WITH aggs AS (
                SELECT
                        order_id,
                        product_id,
                        count(*) AS item_qty,
                        avg(price) AS price,					--  same for all, i.e. avg or min or max
                        avg(freight_value) AS freight_value
                FROM universe.order_items items
                LEFT JOIN universe.orders USING(order_id)
                WHERE TRUE
                AND order_purchase_timestamp < now()
                AND order_purchase_timestamp >
                    (SELECT max(last_updated_timestamp) FROM visible.last_updated WHERE table_name = 'order_items')
                GROUP BY order_id, product_id, order_purchase_timestamp
                --ORDER BY order_id, product_id DESC
        ),
        prep AS (
                SELECT DISTINCT ON (order_id, product_id)	-- results in: select first_value for seller_id and shipping_limit_date
                        order_purchase_timestamp,
                        items.order_id,
                        aggs.item_qty,
                        items.product_id,
                        items.seller_id,
                        items.shipping_limit_date,
                        aggs.price,
                        aggs.freight_value
                FROM universe.order_items items
                LEFT JOIN aggs USING (order_id, product_id)
                LEFT JOIN universe.orders USING (order_id)
                WHERE TRUE
                AND order_purchase_timestamp < now()
                AND order_purchase_timestamp >
                    (SELECT max(last_updated_timestamp) FROM visible.last_updated WHERE table_name = 'order_items')
                ORDER BY order_id, product_id DESC
        )
        SELECT
                order_id,
                item_qty,
                product_id,
                seller_id,
                shipping_limit_date,
                price,
                freight_value
        FROM prep
        ORDER BY order_purchase_timestamp
    """
    rowscount = run_query(conn=conn, query=query, vars=(now,), return_rowscount=True)
    if rowscount != 0: add_entry_to_last_updated_table(conn=conn, table_name='order_items', timestamp=now)





    # customers
    query = """
        LOCK TABLE visible.customers;
        INSERT INTO visible.customers
        SELECT customers.* FROM universe.customers
        LEFT JOIN universe.orders USING(customer_id)
        WHERE TRUE
        AND order_purchase_timestamp < %s
        AND order_purchase_timestamp >
            (SELECT max(last_updated_timestamp) FROM visible.last_updated WHERE table_name = 'customers')
        ORDER BY order_purchase_timestamp DESC
    """
    rowscount = run_query(conn=conn, query=query, vars=(now,), return_rowscount=True)
    if rowscount != 0: add_entry_to_last_updated_table(conn=conn, table_name='customers', timestamp=now)





    # order_reviews
    # to make things simple, transfer all columns according to 'review_answer_timestamp'
    query = """
        LOCK TABLE visible.order_reviews;
        INSERT INTO visible.order_reviews
        SELECT * FROM universe.order_reviews
        WHERE TRUE
        AND review_answer_timestamp <= %s
        AND review_answer_timestamp >
            (SELECT max(last_updated_timestamp) FROM visible.last_updated WHERE table_name = 'order_reviews')
        ORDER BY review_answer_timestamp DESC
    """
    rowscount = run_query(conn=conn, query=query, vars=(now,), return_rowscount=True)
    if rowscount != 0: add_entry_to_last_updated_table(conn=conn, table_name='order_reviews', timestamp=now)


    debug_print(f'update_visible() took {datetime.now() - now}')

    pass



if __name__ == '__main__':
    t0 = datetime.now()
    print("Update visible: Starting...")
    update_visible()
    print(f"Update visible: Done. ({datetime.now() - t0}):" )

import psycopg2
from psycopg2.sql import Identifier, SQL
import pandas as pd

import sys
sys.path.append("..")
from utils.pgfunctions import (
    run_query,
    upload_df_to_table,
    return_df_with_string_columns_without_accents
)

from utils.functions import (
    get_params_from_json_file,
    generate_df_with_random_surnames_and_lastnames,
    capitalize_column_but_keep_fillwords
)

from config.config import (
    PATH_TO_CREDENTIALS,
    SERVER_TO_USE,
    START_DATE,
    CSV_FOLDER,
    REMOVE_FIRST_MONTHS
)

from datetime import datetime, timedelta
import functools
import operator


t0 = datetime.now()
print("Create psql tables: Starting...")


conn_params = get_params_from_json_file(PATH_TO_CREDENTIALS, SERVER_TO_USE)
conn = psycopg2.connect(**conn_params)



# check if table already exists first, if yes abort
# in total should be 19 tables in both schemas
query = """
SELECT * FROM information_schema.tables WHERE(table_schema = 'visible' OR table_schema = 'universe')
"""
rows_count = run_query(conn=conn, query=query, return_rowscount=True)
if rows_count == 19:
    print("Create psql tables: all tables already exist. Skipping...")
    exit()





# Read and Upload CSV files
# Sellers
sellers = pd.read_csv(f'{CSV_FOLDER}/olist_sellers_dataset.csv', delimiter=',')
sellers.isnull().sum()

sellers = return_df_with_string_columns_without_accents(
    sellers,
    [
        'seller_city',
        'seller_state'
    ]
)

sellers = capitalize_column_but_keep_fillwords(sellers, 'seller_city')

# first 10 chars of unique_id suffice to be unique
sellers['seller_id'] = sellers['seller_id'].str[:10]

# create table
query = """
CREATE TABLE IF NOT EXISTS universe.sellers (
        seller_id varchar NOT NULL,
        seller_zip_code_prefix int,
        seller_city varchar,
        seller_state varchar,
        PRIMARY KEY (seller_id)
)
;
"""
run_query(conn=conn, query=query)

# Insert data
upload_df_to_table(conn=conn, df=sellers, schema='universe', table='sellers')





# product categories name translations
prod_cat_translations = pd.read_csv(f'{CSV_FOLDER}/product_category_name_translation.csv', delimiter=',')
prod_cat_translations.isnull().sum()

# create table
query = """
CREATE TABLE IF NOT EXISTS universe.prod_cat_translations (
    product_category_name varchar,
    product_category_name_english varchar,
        PRIMARY KEY (product_category_name)
)
;
"""
run_query(conn=conn, query=query)

# Insert data
upload_df_to_table(conn=conn, df=prod_cat_translations, schema='universe', table='prod_cat_translations')




# customers
customers = pd.read_csv(f'{CSV_FOLDER}/olist_customers_dataset.csv', delimiter=',')
# customers.isnull().sum()
# customers[customers.customer_unique_id.duplicated()]
# customers[customers.customer_id.duplicated()]

customers = return_df_with_string_columns_without_accents(
    customers,
    [
        'customer_city',
        'customer_state'
    ]
)


customers = capitalize_column_but_keep_fillwords(customers, 'customer_city')


# add random names for customer to make data more vivid
unique_customer_ids = customers.customer_unique_id.nunique()
df_names = generate_df_with_random_surnames_and_lastnames(
    unique_customer_ids,
    colnames = ['customer_surname', 'customer_lastname'],
    seed = 42
)
df_names['customer_unique_id'] = customers.customer_unique_id.unique()
customers = customers.merge(df_names, on='customer_unique_id', how='left')

# first 10 chars of unique_id suffice to be unique
customers['customer_unique_id'] = customers['customer_unique_id'].str[:10]
customers['customer_id'] = customers['customer_id'].str[:10]

# Note: each customer_id is assigned on purchase, i.e. this is rather an order_id.
# customer_unqiue_id identifies the repurchases of customers, but is thus not the 'unique' ID in the dataset.

# create table
query = """
CREATE TABLE IF NOT EXISTS universe.customers (
    customer_id varchar,
    customer_unique_id varchar,
    customer_zip_code_prefix int,
    customer_city varchar,
    customer_state varchar,
    customer_surname varchar,
    customer_lastname varchar,
    PRIMARY KEY (customer_id)
)
;
"""
run_query(conn=conn, query=query)

# Insert data
upload_df_to_table(conn=conn, df=customers, schema='universe', table='customers')





# Geolocation
geolocation = pd.read_csv(f'{CSV_FOLDER}/olist_geolocation_dataset.csv', delimiter=',')
# geolocation.isnull().sum()

# Take only one geo coordinate for each zip code prefixes, since can only join on zip code prefixes,
# i.e. more detailed coordinates can't be assigned to a customer anyway (reduces rows from 1M to 19k)
geolocation = geolocation.sort_values(['geolocation_zip_code_prefix',
                                       'geolocation_lat',
                                       'geolocation_lng'
                                       ]).groupby('geolocation_zip_code_prefix', as_index=False).first()

# get rid of accents in names of cities and states
geolocation = return_df_with_string_columns_without_accents(
    geolocation,
    [
        'geolocation_city',
        'geolocation_state'
    ]
)

geolocation = capitalize_column_but_keep_fillwords(geolocation, 'geolocation_city')

# create table
query = """
CREATE TABLE IF NOT EXISTS universe.geolocation (
    geolocation_zip_code_prefix int,
    geolocation_lat numeric,
    geolocation_lng numeric,
    geolocation_city varchar,
    geolocation_state varchar,
    PRIMARY KEY (geolocation_zip_code_prefix)
)
;
"""
run_query(conn=conn, query=query)

# Insert data
upload_df_to_table(conn=conn, df=geolocation, schema='universe', table='geolocation')





# Order items
order_items = pd.read_csv(f'{CSV_FOLDER}/olist_order_items_dataset.csv', delimiter=',')
# order_items.isnull().sum()
# order_items.order_item_id.value_counts()
order_items.rename(columns={'order_item_id':'item_qty'}, inplace=True)

# first 10 chars of unique_id suffice to be unique
order_items['order_id'] = order_items['order_id'].str[:10]
order_items['product_id'] = order_items['product_id'].str[:10]
order_items['seller_id'] = order_items['seller_id'].str[:10]


# Note: Apparently, here we do not have an unqiue ID
#       order_item_id is dropped and instead we aggregate same products, they are counted by item_qty.
#       price is still a unit price: i.e. price * item_qty gives total price for same products
#       same for freight_value

# create table
query = """
CREATE TABLE IF NOT EXISTS universe.order_items (
    order_id varchar,
    item_qty varchar,
    product_id varchar,
    seller_id varchar,
    shipping_limit_date timestamp,
    price numeric,
    freight_value numeric
)
;
"""
run_query(conn=conn, query=query)

# Insert data
upload_df_to_table(conn=conn, df=order_items, schema='universe', table='order_items')





# Order payments
order_payments = pd.read_csv(f'{CSV_FOLDER}/olist_order_payments_dataset.csv', delimiter=',')
# order_payments.isnull().sum()
# order_payments[order_payments.order_id.duplicated()]

# first 10 chars of unique_id suffice to be unique
order_payments['order_id'] = order_payments['order_id'].str[:10]


# Note: Also here, we do not have an unique id

# create table
query = """
CREATE TABLE IF NOT EXISTS universe.order_payments (
    order_id varchar,
    payment_sequential int,
    payment_type varchar,
    payment_installments int,
    payment_value numeric
)
;
"""
run_query(conn=conn, query=query)

# Insert data
upload_df_to_table(conn=conn, df=order_payments, schema='universe', table='order_payments')





# Order reviews
order_reviews = pd.read_csv(f'{CSV_FOLDER}/olist_order_reviews_dataset.csv', delimiter=',')
# order_reviews.isnull().sum()
# order_reviews[order_reviews.order_id.duplicated()]
# order_reviews[order_reviews.review_id.duplicated()]

# first 10 chars of unique_id suffice to be unique
order_reviews['review_id'] = order_reviews['review_id'].str[:10]
order_reviews['order_id'] = order_reviews['order_id'].str[:10]


# Note: Also here, we do not have an unique id

# create table
query = """
CREATE TABLE IF NOT EXISTS universe.order_reviews (
    review_id varchar,
    order_id varchar,
    review_score int,
    review_comment_title varchar,
    review_comment_message varchar,
    review_creation_date timestamp,
    review_answer_timestamp timestamp
)
;
"""
run_query(conn=conn, query=query)

# Insert data
upload_df_to_table(conn=conn, df=order_reviews, schema='universe', table='order_reviews')





# Orders
orders = pd.read_csv(f'{CSV_FOLDER}/olist_orders_dataset.csv', delimiter=',')
# orders.isnull().sum()
# orders[orders.order_id.duplicated()]

# first 10 chars of unique_id suffice to be unique
orders['order_id'] = orders['order_id'].str[:10]
orders['customer_id'] = orders['customer_id'].str[:10]

# create table
query = """
CREATE TABLE IF NOT EXISTS universe.orders (
    order_id varchar,
    customer_id varchar,
    order_status varchar,
    order_purchase_timestamp timestamp,
    order_approved_at timestamp,
    order_delivered_carrier_date timestamp,
    order_delivered_customer_date timestamp,
    order_estimated_delivery_date timestamp,
    PRIMARY KEY (order_id)
)
;
"""
run_query(conn=conn, query=query)

# Insert data
upload_df_to_table(conn=conn, df=orders, schema='universe', table='orders')





# Products
products = pd.read_csv(f'{CSV_FOLDER}/olist_products_dataset.csv', delimiter=',')
products.isnull().sum()
products[products.product_id.duplicated()]

# first 10 chars of unique_id suffice to be unique
products['product_id'] = products['product_id'].str[:10]

# create table
query = """
CREATE TABLE IF NOT EXISTS universe.products (
    product_id varchar,
    product_category_name varchar,
    product_name_lenght int,
    product_description_lenght int,
    product_photos_qty int,
    product_weight_g numeric,
    product_length_cm numeric,
    product_height_cm numeric,
    product_width_cm numeric,
    PRIMARY KEY (product_id)
)
;
"""
run_query(conn=conn, query=query)

# Insert data
upload_df_to_table(conn=conn, df=products, schema='universe', table='products')





# Set all timestamps according to start_date parameter
orders_min = orders.order_purchase_timestamp.min()
add_days = (
    datetime.strptime(START_DATE, '%Y-%m-%d') - \
    datetime.strptime(orders_min, '%Y-%m-%d %H:%M:%S')
).days + 1

timestamps = [
    ('order_items', 'shipping_limit_date'),
    ('order_reviews', 'review_creation_date'),
    ('order_reviews', 'review_answer_timestamp'),
    ('orders', 'order_purchase_timestamp'),
    ('orders', 'order_approved_at'),
    ('orders', 'order_delivered_carrier_date'),
    ('orders', 'order_delivered_customer_date'),
    ('orders', 'order_estimated_delivery_date'),
]

for tab, col in timestamps:
    query = SQL("UPDATE universe.{tab_var} SET {col_var} = {col_var} + INTERVAL '%s days'").format(
        tab_var=Identifier(tab),
        col_var=Identifier(col)
    )
    run_query(conn=conn, query=query, vars=(add_days,))





# Cut first 4 month from history since no real Sales
if REMOVE_FIRST_MONTHS:
    lower_date_bound = (datetime.strptime(START_DATE, '%Y-%m-%d') + timedelta(days=124)).strftime("%Y-%m-%d")

    timestamps = [
        # ('closed_deals', 'won_date'),
        ('order_items', 'shipping_limit_date'),
        ('order_reviews', 'review_creation_date'),
        ('orders', 'order_purchase_timestamp'),
    ]

    for tab, col in timestamps:
        query = SQL("DELETE FROM universe.{tab_var} WHERE {col_var} <= %s").format(
            tab_var=Identifier(tab),
            col_var=Identifier(col)
        )
        run_query(conn=conn, query=query, vars=(lower_date_bound,))





# Create same (empty) tables in visible like in universe
query = "SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname = 'universe';"
tables = pd.DataFrame(run_query(conn=conn, query=query, return_data=True)).to_numpy().tolist()
tables = functools.reduce(operator.iconcat, tables, [])     # unnest list
for tab in tables:
    query = SQL("CREATE TABLE IF NOT EXISTS visible.{tab_var} (like universe.{tab_var} INCLUDING ALL)")\
        .format(
            tab_var=Identifier(tab)
        )
    run_query(conn=conn, query=query)





# create last_updated table
query = """
CREATE TABLE IF NOT EXISTS visible.last_updated (
    table_name varchar,
    last_updated_timestamp timestamp
)
;
"""
run_query(conn=conn, query=query)

# insert start_date for all tables
for tab in tables:  # reuse 'tables' from above
    query = "INSERT INTO visible.last_updated SELECT %s, %s"
    run_query(conn=conn, query=query, vars=(tab, START_DATE))





# Copy tables to visible that are fixed
tables = [
    'geolocation',
    'prod_cat_translations',
    'products',
    'sellers'
]

for tab in tables:
    query = SQL("INSERT INTO visible.{tab_var} SELECT * FROM universe.{tab_var};").format(
        tab_var=Identifier(tab)
    )
    run_query(conn=conn, query=query)




print(f'Create psql tables: Done. ({datetime.now() - t0})')

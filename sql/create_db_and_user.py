import psycopg2
from datetime import datetime
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from psycopg2.sql import (
    SQL,
    Identifier
)


import sys
sys.path.append("..")
from utils.pgfunctions import (
    run_query,
)

from utils.functions import (
    get_params_from_json_file
)

from config.config import (
    PATH_TO_CREDENTIALS,
    SERVER_TO_USE
)


t0 = datetime.now()
print('Create DBs and Users: starting...')




# First, check whether DB olist already exists, if not, create
# Note: use root user credentials as set in credentials.json (and manually on Gitlab as CI var)
conn_params = get_params_from_json_file(PATH_TO_CREDENTIALS, 'pg_root_wo_db')
conn = psycopg2.connect(**conn_params)

# need this to make creation of DBs and USERs possible
# https://stackoverflow.com/questions/34484066/create-a-postgres-database-using-python?noredirect=1&lq=1
conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
cur = conn.cursor()

query = "SELECT datname FROM pg_catalog.pg_database WHERE datname = 'olist'"
rows_count = run_query(conn=conn, query=query, return_rowscount=True)
if rows_count == 0:
    query = "CREATE DATABASE olist"
    cur.execute(query)

cur.close()




# Second, check if schema and roles already exist, if yes, exit script
conn_params = get_params_from_json_file(PATH_TO_CREDENTIALS, "pg_root")
conn = psycopg2.connect(**conn_params)

# check if some of the EXISTS returns an f (false)
query = """
WITH check_existence AS (
        SELECT EXISTS(SELECT SCHEMA_NAME FROM information_schema.schemata WHERE SCHEMA_NAME = 'visible')
    UNION ALL
        SELECT EXISTS(SELECT SCHEMA_NAME FROM information_schema.schemata WHERE SCHEMA_NAME = 'universe')
    UNION ALL
        SELECT EXISTS (SELECT * FROM pg_user WHERE usename = %s)
)
SELECT * FROM check_existence WHERE exists = 'f'
"""

db_user = conn_params['user']
rows_count = run_query(conn=conn, query=query, return_rowscount=True, vars=(db_user,))
if rows_count == 0:
    print("Create DBs and Users: DBs, Schemas, and Users already exist. Skipping...")
    exit()




# create database and user
conn_params = get_params_from_json_file(PATH_TO_CREDENTIALS, "pg_root_wo_db")
conn = psycopg2.connect(**conn_params)
conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

create_user_params = get_params_from_json_file(PATH_TO_CREDENTIALS, SERVER_TO_USE)
create_user = create_user_params['user']
create_password = create_user_params['password']

query = SQL("DROP ROLE IF EXISTS {user_var}").format(user_var=Identifier(create_user))
run_query(conn=conn, query=query)

query = SQL("CREATE USER {user_var}").format(user_var=Identifier(create_user))
run_query(conn=conn, query=query)

query = SQL("ALTER USER {user_var} WITH PASSWORD %s").format(user_var=Identifier(create_user))
run_query(conn=conn, query=query, vars=(create_password,))

query = SQL("GRANT ALL PRIVILEGES ON DATABASE olist TO {user_var}").format(user_var=Identifier(create_user))
run_query(conn=conn, query=query)




# Now, login into DB olist and create schemas
conn_params = get_params_from_json_file(PATH_TO_CREDENTIALS, SERVER_TO_USE)
conn = psycopg2.connect(**conn_params)
conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

create_user_params = get_params_from_json_file(PATH_TO_CREDENTIALS, SERVER_TO_USE)
create_user = create_user_params['user']
create_password = create_user_params['password']

query = "DROP SCHEMA IF EXISTS universe"
run_query(conn=conn, query=query)

query = "CREATE SCHEMA universe"
run_query(conn=conn, query=query)

query = "DROP SCHEMA IF EXISTS visible"
run_query(conn=conn, query=query)

query = "CREATE SCHEMA visible"
run_query(conn=conn, query=query)

query = SQL("GRANT ALL PRIVILEGES ON SCHEMA universe TO {user_var}").format(user_var=Identifier(create_user))
run_query(conn=conn, query=query)

query = SQL("GRANT ALL PRIVILEGES ON SCHEMA visible TO {user_var}").format(user_var=Identifier(create_user))
run_query(conn=conn, query=query)


print(f'Create DBs and Users: Done. ({datetime.now() - t0})')

import dash
from layout.layout import layout
from pages.daily.daily_data import cache
import dash_bootstrap_components as dbc
import flask
from flask import session
from utils.functions import (
    generate_unique_identifier,
    debug_print,
)
from config.config import (
    APP_TITLE
)

# Make sure visible DB has fresh data.
# This is simulated here.
# This would actually happen independently on the
# server where live data is collected ongoing.
from sql.update_visible import update_visible
update_visible()


# https://www.bootstrapcdn.com/
# see more themes here: https://dash-bootstrap-components.opensource.faculty.ai/docs/themes/
# test themes here: https://hellodash.pythonanywhere.com/theme_explorer
#                   https://github.com/AnnMarieW/dash-bootstrap-templates
EXTERNAL_STYLESHEET = [dbc.themes.BOOTSTRAP, dbc.icons.FONT_AWESOME]

CACHE_CONFIG = {
    'CACHE_TYPE': 'FileSystemCache',
    'CACHE_DIR': '_flask_cache',
    'CACHE_THRESHOLD': 200,
    'CACHE_DEFAULT_TIMEOUT': 86400,
}


server = flask.Flask(__name__)
server.config['SECRET_KEY'] = 'use_some_fixed_secret_key_05152538451dasda4d21'

cache.init_app(server, config=CACHE_CONFIG)


app = dash.Dash(
    __name__,
    suppress_callback_exceptions=True,
    title=APP_TITLE,
    external_stylesheets= EXTERNAL_STYLESHEET,
    server = server
)





@server.route('/')
def serve_layout():
    if 'session_id' in session:
        session_id = session.get('session_id')
        debug_print(f'Old session_id is used: {session_id}')
    else:
        session_id = generate_unique_identifier()
        session['session_id'] = session_id
        debug_print(f'New session_id created: {session_id}')
    return layout


app.layout = serve_layout





# shutdown route to shutdown server remotely, used during testing
def stop_server():
    # https://werkzeug.palletsprojects.com/en/0.15.x/serving/#shutting-down-the-server
    stopper = flask.request.environ.get("werkzeug.server.shutdown")
    if stopper is None:
        raise RuntimeError("Not running with the Werkzeug Server")
    stopper()
    return "Flask server is shutting down"


@server.route('/shutdown', methods=('POST',))
def shutdown_server():
    return stop_server()

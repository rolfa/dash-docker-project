import yaml
import pathlib
import os

PATH = pathlib.Path(__file__).parent
CONFIG_PATH = PATH.joinpath("config.yaml").resolve()


DEFAULT_CONFIG = dict(
    pgsql_server = dict(
        PATH_TO_CREDENTIALS = PATH.joinpath("default_credentials.json"),
        SERVER_TO_USE = "user"
    ),
    flask_server = dict(
        PORT = 8050,
        DEBUG = True
    ),
    create_data = dict(
        START_DATE = "2020-09-04",
        CSV_FOLDER = "/data",
        REMOVE_FIRST_MONTHS = True
    ),
    debug = dict(
        DEBUG_PRINT = False
    ),
    dash_app = dict(
        APP_TITLE = 'Daily Dashboard'
    )
)


# when no config.yaml exists, use DEFAULT_CONFIG
if CONFIG_PATH.is_file():
    with open(CONFIG_PATH, "r") as f:
        config = yaml.safe_load(f)
else:
    config = DEFAULT_CONFIG


# check if environment variables are set and override
if os.environ.get("PATH_TO_CREDENTIALS") is not None:
    config["pgsql_server"]["PATH_TO_CREDENTIALS"] = os.environ.get("PATH_TO_CREDENTIALS")
if os.environ.get("SERVER_TO_USE") is not None:
    config["pgsql_server"]["SERVER_TO_USE"] = os.environ.get("SERVER_TO_USE")
if os.environ.get("FLASK_SERVER_DEBUG") is not None:
    config["flask_server"]["DEBUG"] = os.environ.get("FLASK_SERVER_DEBUG").lower() in ('true', '1')
if os.environ.get("DEBUG_PRINT") is not None:
    config["debug"]["DEBUG_PRINT"] = os.environ.get("DEBUG_PRINT").lower() in ('true', '1')
if os.environ.get("APP_TITLE") is not None:
    config["dash_app"]["APP_TITLE"] = os.environ.get("APP_TITLE")


# pgsql_sever
PATH_TO_CREDENTIALS: str = str(config["pgsql_server"]["PATH_TO_CREDENTIALS"])
SERVER_TO_USE: str = str(config["pgsql_server"]["SERVER_TO_USE"])


# flask_server
PORT: str = str(config["flask_server"]["PORT"])
DEBUG: bool = bool(config["flask_server"]["DEBUG"])


#create_data
START_DATE: str = str(config["create_data"]["START_DATE"])
CSV_FOLDER: str = str(config["create_data"]["CSV_FOLDER"])
REMOVE_FIRST_MONTHS: bool = bool(config["create_data"]["REMOVE_FIRST_MONTHS"])

# debug
DEBUG_PRINT: bool = bool(config["debug"]["DEBUG_PRINT"])

# dash_app
APP_TITLE: str = str(config["dash_app"]["APP_TITLE"])


from app import app, server

from pages.daily.daily_callbacks import (
    # show_clickData_output,
    make_bar_1w_chart,
    make_line_18m_chart,
    init_orders_table_container_at_startup,
    make_customer_or_order_summary_table
)

from config.config import (
    PORT,
    DEBUG
)

if __name__ == '__main__':
    app.run_server(
        port=PORT,
        debug=DEBUG,
        host='0.0.0.0'
    )

from dash import html
import dash_bootstrap_components as dbc
from dash import dash_table


# Cards
class card_header:
    def __init__(self, id) -> None:
        self.id = id
        self.card = dbc.Card(
            html.H5([]),
            style={'text-align': 'left'},
            body=True,
            id=self.id
        )

    def create(self) -> dbc.Card:
        return self.card





class card_header_emphasize:
    def __init__(self, id) -> None:
        self.id = id
        self.card = dbc.Card(
            html.H5([]),
            style={'text-align': 'left', },
            body=True,
            color='dark',
            outline= True,
            id = self.id
        )

    def create(self) -> dbc.Card:
        return self.card





# Tables
class table_basic:
    def __init__(self, id) -> None:
        self.id = id
        self.table = dash_table.DataTable(
            id=self.id,
            columns=[],
            data=[],
            style_table={'overflowY': 'auto'},
            fixed_rows={'headers': True},
            style_cell={
                'minWidth': 90,
                'maxWidth': 0,
                'font-family':'sans-serif',
                'textOverflow': 'ellipsis',
                'overloadflow': 'hidden',
            },
            page_size=4,
            sort_action="native",
            sort_mode="single",
        )

    def create(self) -> dash_table.DataTable:
        return self.table

from dash import dcc
from dash import html
import dash_bootstrap_components as dbc

from pages.daily.daily_graphs import (
    generate_controls_sales_rev,
    generate_modal_to_confirm_cache_refresh,
)



layout = dbc.Container(
    fluid=True,
    children=[
        # Last updated
        dbc.Row(
            dbc.Col(
                # width={"size": 2, "offset": 10},
                children=[
                dbc.Card(
                    id='graph_and_dropdown_last_updated',
                    children=[],
                    style={'text-align':'right'},
                    color='info', inverse=True,
                    body=True)
            ])
        ),
        dbc.Modal(
            id="modal_confirm_refresh_all",
            children=generate_modal_to_confirm_cache_refresh(),
            centered=True,
            is_open=False
        ),
        html.Br(),

        # Header('Daily Live Dashboard', app),
        dbc.Row(
            [
                # Single value graphs
                dbc.Col(
                    md=2,
                    # together with mt-auto below makes Div align at bottom
                    className='d-flex flex-column',
                    children=[
                        html.H4('Today until now'),
                        # https://getbootstrap.com/docs/4.0/utilities/spacing/
                        dbc.Card(id='sales_sv', children=[], className='mb-2 text-center'),
                        dbc.Card(id='revenue_sv', children=[], className='mb-2 text-center'),
                        dbc.Card(id='new_customer_ratio_sv', children=[], className='mb-2 text-center'),
                        html.Br(),
                        html.Div(generate_controls_sales_rev(), className='mt-auto')
                    ]
                ),

                # Bar Plot
                dbc.Col(
                    md=5,
                    children=[
                        html.H4("Orders last week"),
                        dbc.Card([dbc.CardBody([dcc.Graph(id="bar_1w", figure={})])])
                    ]
                ),

                # Line Plot
                dbc.Col(
                    md=5,
                    children=[
                        html.H4("Orders last 18 months"),
                        dbc.Card([dbc.CardBody([dcc.Graph(id="line_18m", figure={})])])
                    ]
                )
            ]
        ),

        html.Br(),

        dbc.Row(
            [
                # Map
                dbc.Col(
                    md=7,
                    children=[
                        html.H4('Orders on map'),
                        dbc.Card(dcc.Graph(id='map_orders', figure={}), body=True)
                    ]
                ),

                # Tables
                dbc.Col(
                    md=5,
                    children=[
                        html.H4('Orders per city'),
                        html.Div(id='table_orders_div_container'),
                        html.Br(),
                        html.H4(id='table_customer_or_order_header'),
                        html.Div(id='table_customer_or_order_div_container')
                    ]
                )
            ]
        ),

        # dbc.Row(
        #     [
        #         html.Pre(id='show_click_output', style={'border': 'thin lightgrey solid', 'overflowX': 'scroll'}),
        #         html.Pre(id='show_click_output2', style={'border': 'thin lightgrey solid', 'overflowX': 'scroll'}),
        #     ],
        # ),

        # signal value to trigger callbacks
        dcc.Store(id='trigger_past_data_fetching'),
        dcc.Store(id='trigger_today_data_fetching'),
        dcc.Store(id='trigger_today_data_fetching2'),
        dcc.Store(id='trigger_modal_to_confirm_refresh_cache_to_close'),
        dcc.Store(id='trigger_modal_to_confirm_refresh_cache_to_close2'),
        dcc.Store(id='trigger_interval_trigger_today_data_refresh'),
        dcc.Store(id='trigger_interval_trigger_today_data_refresh2'),

        # local stores
        dcc.Store(id='local_store_today_data'),
        dcc.Store(id='local_store_item_today_data'),

        # interval component to trigger data refresh
        dcc.Interval(id='interval_trigger_today_data_refresh', n_intervals=0, interval=60000),

        # hidden div
        # https://community.plotly.com/t/app-callback-without-an-output/5502/37
        html.Div(id='hidden_div_for_empty_input', children=[], hidden=True),
    ]
)

import pandas as pd
import numpy as np
from datetime import datetime
import warnings

from utils.functions import (
    debug_print,
    get_value_by_colname_from_one_row_df_or_0_if_empty,
    get_date_for_n_days_in_the_past_as_str
)

from utils.pgfunctions import (
    get_last_updated_value
)

from utils.functions import (
    make_nice_table_header_from_colnames,
    is_json,
    is_DataFrame,
    convert_json_to_df,
)

from pages.daily.daily_data import (
    load_past_data_from_global_cache,
    load_item_past_data_from_global_cache,
    aggregate_data_by_date,
    aggregate_data_by_city,
    aggregate_items_by_order_id,
    append_todays_data_to_past_data,
    total_orders_by_customer,
#    session_id,
    cache
)

from pages.templates import dash_templates

from typing import (
    TypedDict,
    Optional,
    Union
)

from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
from dash import dash_table
import plotly.graph_objects as go
import plotly.express as px
import plotly.io as pio


# set default plotly template
# show all available tempaltes with: pio.templates
pio.templates.default = "plotly_white"
# pio.templates.default = "plotly_dark"


# get tempalte formats for data table
FT_money = dash_table.FormatTemplate.money(2)





# graphs
def generate_graph_and_dropdown_last_updated(
        session_id: str = None,
        display_update_interval: Optional[str] = None,
):
    # for debugging, comment later
    display_update_interval = cache.cache.get(f'{session_id}_display_update_interval')
    update_interval = cache.cache.get(f'{session_id}_update_interval')
    debug_print(
        f'generate_graph_and_dropdown_last_update: {session_id}: {update_interval}, {display_update_interval}'
    )

    display_update_interval = cache.cache.get(f'{session_id}_display_update_interval')

    # input from not yet clicked button is None
    if display_update_interval is None:
        display_update_interval = 'Update interval: 1 min'

    newest_datapoint_in_db = get_last_updated_value()
    last_refereshed_timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return [
        dcc.Markdown(
            id='last_updated_sv',
            children = '**Newest datapoint**: ' + str(newest_datapoint_in_db) + ' | ' + \
                '**Last refresh**: ' +  str(last_refereshed_timestamp),
            className='d-inline-block',
        ),
        dbc.DropdownMenu(
            id='dropdown_last_updated',
            label="",
            # class_name='input-group-btn',
        class_name='d-inline-block',
            style={'margin-left': '5px'},
            size='sm',
            children=[
                dbc.DropdownMenuItem('Refresh now',id='refresh_now_update_visible'),
                dbc.DropdownMenuItem(divider=True),
                dbc.DropdownMenuItem(
                    id='display_current_update_interval',
                    children=display_update_interval,
                    header=True
                ),
                dbc.DropdownMenuItem(divider=True),
                dbc.DropdownMenuItem("10 sec", id='today_refresh_interval_10s'),
                dbc.DropdownMenuItem("30 sec", id='today_refresh_interval_30s'),
                dbc.DropdownMenuItem("1 min", id='today_refresh_interval_1m', active=False),
                dbc.DropdownMenuItem("5 min", id='today_refresh_interval_5m'),
                dbc.DropdownMenuItem("10 min", id='today_refresh_interval_10m'),
                dbc.DropdownMenuItem(
                    toggle=False,
                    children=[
                        dbc.ButtonGroup(
                            [
                                dbc.Input(
                                    id='today_refresh_interval_custom_number',
                                    type='number',
                                    value='5',
                                    size='sm',
                                    style={'width': '60px'},
                                ),
                                dbc.Select(
                                    id='today_refresh_interval_custom_unit',
                                    value='min',
                                    style={'width': '80px'},
                                    options=[
                                        {'label': 'h', 'value': 'h'},
                                        {'label': 'min', 'value': 'min'},
                                        {'label': 'sec', 'value': 'sec'}
                                    ]
                                )
                            ]
                        ),
                        dbc.Button("Set", color="primary", id='today_refresh_interval_custom_btn',),
                    ]
                ),
                dbc.DropdownMenuItem(divider=True),
                dbc.DropdownMenuItem(
                    dbc.Button(
                        'Clear Cache',
                        color='danger',
                        # className='mx-auto d-block'
                        className='d-grid gap-2 mx-auto d-block col-12',
                        id='btn_refresh_all',
                        n_clicks=0,
                    ),
                )
            ]
        )
    ]





# notification to confirm cache refresh
def generate_modal_to_confirm_cache_refresh():
    return [
        dbc.ModalHeader(dbc.ModalTitle("Clear Global Cache"), close_button=True),
        dbc.ModalBody(
            [
                "Are you sure you want to clear the global cache?",
            ]
        ),
        dbc.ModalFooter(
            dbc.Button(
                "Confirm",
                color="danger",
                id="btn_confirm_refresh_all",
                className="mx-auto center",
                n_clicks=0,
            )
        )
    ]





def generate_graph_sales_sv(input_from_local_store_today_data):
    df_today = pd.read_json(input_from_local_store_today_data, orient='split')
    agg_by_date_today = aggregate_data_by_date(df_today)
    return [
        dbc.CardBody(
            [
                html.H5(
                    get_value_by_colname_from_one_row_df_or_0_if_empty(agg_by_date_today, 'sales'),
                    className='card-title'
                ),
                html.P("Sales", className='card-text')
            ]
        )
    ]





def generate_graph_revenue_sv(input_from_local_store_today_data):
    df_today = pd.read_json(input_from_local_store_today_data, orient='split')
    agg_by_date_today = aggregate_data_by_date(df_today)
    return [
        dbc.CardBody(
            [
                html.H5(
                    "${:,.0f}".format(
                        get_value_by_colname_from_one_row_df_or_0_if_empty(agg_by_date_today, 'revenue')
                    ),
                    className='card-title'
                ),
                html.P('Revenue', className='card-text')
            ]
        )
    ]





def generate_graph_new_customer_ratio_sv(input_from_local_store_today_data):
    df_today = pd.read_json(input_from_local_store_today_data, orient='split')
    df_past = load_past_data_from_global_cache()
    df_all = append_todays_data_to_past_data(df_past, df_today)
    df_total_orders = total_orders_by_customer(df_all)
    customer_ids_today = df_today['customer_id'].unique()
    df_total_orders_today = df_total_orders[df_total_orders['customer_id'].isin(customer_ids_today)]
    new_customer_ratio = len(df_total_orders_today.query('total_orders == 1')) / len(df_total_orders_today)
    return [
        dbc.CardBody(
            [
                html.H5(
                    str(round(new_customer_ratio)*100) + '%',
                    className='card-title'
                ),
                html.P("New Customer Ratio", className='card-text')
            ]
        )
    ]





def generate_controls_sales_rev():
    return [
        dbc.Button(
            id='btn_show_graph_config',
            children="Show graph config",
            color="secondary",
            outline=True,
            # className="d-grid gap-2 mx-auto",
            size='sm'
        ),

        dbc.Collapse(
            id='collapse_graph_control',
            is_open=False,
            children=[
                dbc.Card(
                    dbc.CardBody(
                        [
                            dcc.Dropdown(
                                id='dropdown_toggle_sales_rev',
                                options=[
                                    {'label': 'Show Sales', 'value': 'sales'},
                                    {'label': 'Show Revenue', 'value': 'revenue'}
                                ],
                                value='sales',
                            ),
                            dbc.Checklist(
                                id='checklist_graphs_show_only_for_selected_city',
                                options=[
                                    {
                                        'label': 'Filter selected city',
                                        'value': True
                                    }
                                ],
                                value=[],
                            )
                        ]
                    ),
                )
            ]
        )
    ]





# graph sales week bar
def create_pure_graph_sales_week_bar(
        data: pd.DataFrame,
        x: str = 'dow_short',
        y: str = 'sales'
) -> None:  # plotly.graph_objects.Figure returns None
    fig = px.bar(
        data,
        x=x,
        y=y,
        labels={
            'dow_short': 'Day of Week',
            'sales': 'Sales',
            'revenue': 'Revenue'
        }
    )

    fig.update_layout(
        autosize=False,
        # width=500,
        height=310,
    )

    if y == 'sales':
        fig.update_traces(
            hovertemplate='<b>%{y:,.0f}</b>'
        )
    else:
        fig.update_traces(
            hovertemplate='<b>$%{y:,.0f}</b>'
        )

    return fig


def generate_graph_sales_week_bar(
        input_from_local_store_today_data: TypedDict,
        x: str = 'dow_short',
        y: str = 'sales',
        city_filter: Optional[str] = None
) -> None:
    df_today = pd.read_json(input_from_local_store_today_data, orient='split')
    df_past = load_past_data_from_global_cache()
    df_all = append_todays_data_to_past_data(df_past, df_today)

    if city_filter is not None:
        df_all = df_all[df_all['city'] == city_filter]

    last_week = get_date_for_n_days_in_the_past_as_str(6)
    agg_by_date_week = aggregate_data_by_date(df_all[df_all.date >= last_week])


    return create_pure_graph_sales_week_bar(data=agg_by_date_week, x=x, y=y)





def generate_graph_sales_18m_line(
        input_from_local_store_today_data: TypedDict,
        x: str = 'date',
        y: str = 'sales',
        city_filter: Optional[str] = None
) -> None:
    df_today = pd.read_json(input_from_local_store_today_data, orient='split')
    df_past = load_past_data_from_global_cache()
    df_all = append_todays_data_to_past_data(df_past, df_today)

    if city_filter is not None:
        df_all = df_all[df_all['city'] == city_filter]

    last_18m = get_date_for_n_days_in_the_past_as_str(547)
    agg_by_date_18m = aggregate_data_by_date(df_all[df_all.date >= last_18m])

    fig = px.line(
        agg_by_date_18m,
        x=x,
        y=y,
        labels={
            'date': 'Date',
            'sales': 'Sales',
            'revenue': 'Revenue'
        },
    )

    fig.add_trace(
        go.Scatter(
            x=agg_by_date_18m[x],
            y=agg_by_date_18m[y].rolling(window=7).mean(),
            mode='lines',
            name='')
    )

    fig.update_layout(
        # autosize=True,
        # width=500,
        height=310,
        showlegend=False
    )

    if y == 'sales':
        fig.update_traces(
            hovertemplate=
            '<b>%{y:,.0f}</b><br>' +
            '%{x|%a}, %{x}',
        )
    else:
        fig.update_traces(
            hovertemplate=
            '<b>$%{y:,.0f}</b><br>' +
            '%{x|%a}, %{x}',
        )

    return fig





def generate_graph_orders_map(input_from_local_store_today_data: TypedDict):
    df_today = pd.read_json(input_from_local_store_today_data, orient='split')
    agg_by_city_today = aggregate_data_by_city(df_today)
    ## https://plotly.com/python/hover-text-and-formatting/
    orders_map = px.scatter_mapbox(
        agg_by_city_today,
        lat="lat",
        lon="lng",
        hover_name="city",
        # hover_data=['sales', 'revenue'],
        hover_data={
            'sales': True,
            'revenue': True,
            'lat': False,
            'lng': False
        },
        zoom=4,
        height=900,
        #color_discrete_sequence=["fuchsia"]
    )

    ## https://plotly.com/python/reference/scattermapbox/
    orders_map.update_layout(
        mapbox_style="open-street-map",
        margin={
            "r":0,
            "t":0,
            "l":0,
            "b":0
        },
    )

    # see 'go' to see why refering to it like this makes sense
    orders_map.data[0].update(
        marker={
            'opacity': 0.6,
            # 'size': np.log2(agg_by_city_today['sales'].values + 1) + 5,
            'size': np.where(
                agg_by_city_today['sales'] > 15,
                15,
                agg_by_city_today['sales']
            ) + 7,
            #'sizemin': 4,
            'sizeref': 0.75,
            'allowoverlap': True
        }
    )

    return orders_map





class cls_table_orders_container(object):

    def __init__(self):

        self.data = None
        self.df = None

        self.city_filter = None

        self.varlist = [
            'id',
            'time',
            'order_id',
            'customer_id',
            'surname',
            'lastname',
            'payment_value'
        ]

        # cards
        self.card_city = dash_templates.card_header_emphasize(id='table_orders_card_city').create()
        self.card_sales = dash_templates.card_header(id='table_orders_card_sales').create()
        self.card_revenue = dash_templates.card_header(id='table_orders_card_revenue').create()

        # orders table
        self.additional_params = [{'id': 'payment_value', 'format': FT_money, 'type': 'numeric'}]
        self.table_orders = dash_templates.table_basic(id='table_orders').create()
        self.table_orders.page_size = 10
        self.table_orders.sort_by=[{'column_id': 'time', 'direction': 'desc'}]


    def _create_initial_df(self, data: Optional[Union[TypedDict, pd.DataFrame]]) -> pd.DataFrame:
        if is_json(data):
            return convert_json_to_df(data)
        elif is_DataFrame(data):
            return data
        else:
            raise TypeError("json or DataFrame allowed for argument data")

    def _filter_df_by_city(self, city_filter: Optional[str]) -> pd.DataFrame:
        return self.df[self.df['city'] == city_filter].sort_values(by='time')

    def create_df(self, city_filter: Optional[str] = None):
        if city_filter is None:
            city_filter = self.city_filter
        self.df = self._create_initial_df(self.data)
        self.df = self._filter_df_by_city(city_filter)

    def create_table_df(self, varlist: Optional[list] = None):
        if varlist is None:
            varlist = self.varlist
        self.table_df = self.df[varlist]

    def update_table_orders(self):
        self.table_orders.data = self.table_df.to_dict('records')
        self.table_orders.columns = make_nice_table_header_from_colnames(
                self.table_df.columns,
                additional_params=self.additional_params
            )

    def update_data(self, new_data: Optional[Union[TypedDict, pd.DataFrame]], city_filter: Optional[str]):
        self.city_filter = city_filter
        self.data = new_data
        self.create_df()
        self.update_card_city()
        self.update_card_sales()
        self.update_card_revenue()
        self.create_table_df()
        self.update_table_orders()

    def return_elemets_for_callback(self):
        """
        Returns 4 values:
        - self.card_city.children
        - self.card_sales.children
        - self.card_revenue.children
        - self.table_orders.data
        - self.table_orders.columns
        """
        return self.card_city.children, \
            self.card_sales.children, \
            self.card_revenue.children, \
            self.table_orders.data, \
            self.table_orders.columns
            # self.table_df.to_dict('records')


    def _calc_sales(self) -> int:
        return len(self.df)

    def _calc_revenue(self) -> int:
        try:
            revenue = self.df['payment_value'].sum()
            return revenue
        except Exception:
            warnings.warn("WARNING: column payment_value to filter for revenue does not exist.")
            return 99999999

    def _get_corresponding_state(self) -> str:
        try:
            state = self.df['state'].head(1).item()
            return state
        except Exception:
            warnings.warn(
                "WARNING: state is missing for current city_filter or column state does not exist."
            )
            return ""

    def update_card_city(self):
        state = self._get_corresponding_state()
        display_city_and_state = self.city_filter + ', ' + str(state)
        self.card_city.children = html.H5([display_city_and_state])

    def update_card_sales(self):
        sales = self._calc_sales()
        display_sales = 'Sales: ' + str(sales)
        self.card_sales.children = html.H5([display_sales])

    def update_card_revenue(self):
        revenue = self._calc_revenue()
        display_revenue = 'Revenue: ' +  "${:,.2f}".format(revenue)
        self.card_revenue.children = html.H5([display_revenue])

    # create final dash container
    def return_table_orders_container(self):
        return html.Div(
            [
                dbc.Card(
                    dbc.CardBody(
                        id = 'table_orders_container',
                        children=[
                            dbc.Row(
                                [
                                    dbc.Col(self.card_city, width=5),
                                    dbc.Col(self.card_sales, width=3),
                                    dbc.Col(self.card_revenue, width=4)
                                ]
                            ),
                            html.Br(),
                            dbc.Row(self.table_orders)
                        ]
                    )
                )
            ]
        )





class cls_table_order_details_container(object):

    def __init__(self) -> None:

        self.data_orders = None
        self.data_items = None

        self.df = None
        self.table_df = None
        self.header_df = None

        self.order_id = None

        self.header_varlist = [
                'city',
                'order_id',
                'surname',
                'lastname',
                'product_id',
                'category',
                'item_qty',
                'price'
            ]

        self.table_varlist = [
            'product_id',
            'category',
            'item_qty',
            'price'
        ]

        # card
        self.card_name = dash_templates.card_header_emphasize(id='table_order_details_card_name').create()

        # table
        self.additional_params = [{'id': 'price', 'format': FT_money, 'type': 'numeric'}]
        self.table_order_details = dash_templates.table_basic(id='table_customer_or_order').create()


    def _create_initial_df(self, data: Optional[Union[TypedDict, pd.DataFrame]]) -> pd.DataFrame:
        if is_json(data):
            return convert_json_to_df(data)
        elif is_DataFrame(data):
            return data
        else:
            raise TypeError("json or DataFrame allowed for argument data")

    def filter_df_by_order_id(self, df: Optional[pd.DataFrame], order_id: Optional[str]) -> pd.DataFrame:
        if order_id is None:
            warnings.warn('WARNING: order_id is empty in self.filter_df_by_order_id().')
        return df[df['order_id'] == order_id]

    def reduce_df_columns_to_varlist(self, varlist: Optional[list]) -> pd.DataFrame:
        return self.df[varlist]

    def create_df(self, order_id: Optional[str] = None):
        if order_id is None:
            order_id = self.order_id
        _df_orders = self._create_initial_df(self.data_orders)
        _df_orders = self.filter_df_by_order_id(_df_orders, order_id)
        _df_items = self._create_initial_df(self.data_items)
        _df_items = self.filter_df_by_order_id(_df_items, order_id)
        self.df = _df_orders.merge(_df_items,on='order_id', how='right')

    def create_table_df(self, varlist: Optional[list] = None):
        if varlist is None:
            varlist = self.table_varlist
        self.table_df = self.reduce_df_columns_to_varlist(varlist)

    def create_header_df(self, varlist: Optional[list] = None):
        if varlist is None:
            varlist = self.header_varlist
        self.header_df = self.reduce_df_columns_to_varlist(varlist)

    def update_table_order_details(self):
        self.table_order_details.data = self.table_df.to_dict('records')
        self.table_order_details.columns = make_nice_table_header_from_colnames(
                self.table_df.columns,
                additional_params=self.additional_params
            )

    def update_card_name(self):
        lastname = str(self.df['lastname'].head(1).item())
        surname = str(self.df['surname'].head(1).item())
        self.card_name.children = html.H5([lastname + ', ' + surname])

    def update_data(
            self,
            new_data_orders: Optional[Union[TypedDict, pd.DataFrame]],
            new_data_items: Optional[Union[TypedDict, pd.DataFrame]],
            order_id: str
    ):
        self.order_id = order_id
        self.data_orders = new_data_orders
        self.data_items = new_data_items
        self.create_df()
        self.update_card_name()
        self.create_table_df()
        self.update_table_order_details()

    def return_elemets_for_callback(self):
        """
        Returns 3 values:
        - self.card_name.children
        - self.table_order_details.data
        - self.table_order_details.columns
        """
        return self.card_name.children, \
            self.table_order_details.data, \
            self.table_order_details.columns

    # create final dash container
    def return_table_order_details_container(self):
        return (
            'Order details',
            dbc.Card(
                dbc.CardBody(
                    id='table_order_details_container',
                    children=[
                        dbc.Row(dbc.Col(self.card_name, width=6)),
                        html.Br(),
                        dbc.Row(self.table_order_details)
                    ]
                )
            )
        )





class cls_table_customer_details_container(object):

    def __init__(self) -> None:

        self.data_orders = None
        self.data_items = None

        self._df_orders_past = load_past_data_from_global_cache()
        self._df_items_past = load_item_past_data_from_global_cache()

        self.df = None
        self.table_df = None

        self.order_id = None
        self.derived_customer_id = None

        self.header_varlist = [
            'customer_id',
            'order_id',
            'order_status',
            'payment_value',
            'city',
            'state',
            'date',
            'time',
            'surname',
            'lastname'
        ]

        self.table_varlist = [
            'date',
            'time',
            'order_id',
            'payment_value',
            'total_items',
            'total_qty',
            'order_status'
        ]

        # cards
        self.card_name = dash_templates.card_header_emphasize(id='table_customer_card_name').create()
        self.card_customer_id = dash_templates.card_header(id='table_customer_card_customer_id').create()
        self.card_total_orders = dash_templates.card_header(id='table_customer_card_total_orders').create()
        self.card_total_revenue = dash_templates.card_header(id='table_customer_card_total_revenue').create()
        self.card_total_avg_basket = dash_templates.card_header(id='table_customer_card_total_avg_basket').create()

        # table
        self.additional_params = [{'id': 'payment_value', 'format': FT_money, 'type': 'numeric'}]
        self.table_customer_details = dash_templates.table_basic(id='table_customer_or_order').create()


    def _create_initial_df(self, data: Optional[Union[TypedDict, pd.DataFrame]]) -> pd.DataFrame:
        if is_json(data):
            return convert_json_to_df(data)
        elif is_DataFrame(data):
            return data
        else:
            raise TypeError("json or DataFrame allowed for argument data")

    def create_df(self, order_id: Optional[str] = None):
        if order_id is None:
            order_id = self.order_id
        _df_orders = self._create_initial_df(self.data_orders)
        _df_orders_all = append_todays_data_to_past_data(self._df_orders_past, _df_orders)
        _df_orders_all = _df_orders_all[self.header_varlist]
        _df_items_today = self._create_initial_df(self.data_items)
        _df_items_all = append_todays_data_to_past_data(self._df_items_past, _df_items_today)
        _df_items_all_agg = aggregate_items_by_order_id(_df_items_all)
        _df_single_customer = _df_orders_all.merge(_df_items_all_agg, how = 'left', on = 'order_id')
        self.derived_customer_id = _df_orders.at[order_id, 'customer_id']
        self.df = _df_single_customer[_df_single_customer['customer_id'] == self.derived_customer_id]
        self.df = self.df.sort_values(['date', 'time'], ascending=False)

    def create_table_df(self, varlist: Optional[list] = None):
        if varlist is None:
            varlist = self.table_varlist
        self.table_df = self.df[varlist]

    def update_table_customer_details(self):
        self.table_customer_details.data = self.table_df.to_dict('records')
        self.table_customer_details.columns = make_nice_table_header_from_colnames(
                self.table_df.columns,
                additional_params=self.additional_params
        )

    def update_card_name(self):
        lastname = str(self.df['lastname'].head(1).item())
        surname = str(self.df['surname'].head(1).item())
        self.card_name.children = html.H5([lastname + ', ' + surname])

    def update_card_customer_id(self):
        self.card_customer_id.children = html.H5(self.derived_customer_id)

    def update_card_total_orders(self):
        _total_orders = len(self.df[self.df['order_status'] == 'delivered'])
        self.card_total_orders.children = html.H5('Total orders: ' + str(_total_orders))

    def update_card_total_revenue(self):
        _total_revenue = self.df[self.df['order_status'] == 'delivered']['payment_value'].sum()
        self.card_total_revenue.children = html.H5('Total revenue: ' + "${:,.2f}".format(_total_revenue))

    def update_card_total_avg_basket(self):
        _total_avg_basket = self.df[self.df['order_status'] == 'delivered']['payment_value'].mean()
        self.card_total_avg_basket.children = html.H5('Avg basket: ' + "${:,.2f}".format(_total_avg_basket))

    def update_data(
            self,
            new_data_orders: Optional[Union[TypedDict, pd.DataFrame]],
            new_data_items: Optional[Union[TypedDict, pd.DataFrame]],
            order_id: str
    ):
        self.order_id = order_id
        self.data_orders = new_data_orders
        self.data_items = new_data_items
        self.create_df()
        self.update_card_name()
        self.update_card_customer_id()
        self.update_card_total_orders()
        self.update_card_total_revenue()
        self.update_card_total_avg_basket()
        self.create_table_df()
        self.update_table_customer_details()

    def return_elemets_for_callback(self):
        """
        Returns 7 values:
        - self.card_name.children
        - self.card_customer_id.children
        - self.card_total_orders.children
        - self.card_total_revenue.children
        - self.card_total_avg_basket.children
        - self.table_customer_details.data
        - self.table_customer_details.columns
        """
        return self.card_name.children, \
            self.card_customer_id.children, \
            self.card_total_orders.children, \
            self.card_total_revenue.children, \
            self.card_total_avg_basket.children, \
            self.table_customer_details.data, \
            self.table_customer_details.columns

    # create final dash container
    def return_table_customer_details_container(self):
        return (
            'Customer details',
            dbc.Card(
                dbc.CardBody(
                    id='table_customer_details_container',
                    children=[
                        dbc.Row(
                            [
                                dbc.Col(self.card_name, width=6),
                                dbc.Col(self.card_customer_id, width=6)
                            ]
                        ),
                        html.Br(),
                        dbc.Row(
                            [
                                dbc.Col(self.card_total_orders, width=4),
                                dbc.Col(self.card_total_revenue, width=4),
                                dbc.Col(self.card_total_avg_basket, width=4)
                            ]
                        ),
                        html.Br(),
                        dbc.Row(self.table_customer_details)
                    ]
                )
            )
        )

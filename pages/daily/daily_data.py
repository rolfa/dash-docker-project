import psycopg2
import pandas as pd
from datetime import datetime

from typing import (
    Optional
)

from utils.functions import (
    debug_print,
    get_params_from_json_file,
)

from utils.pgfunctions import (
    run_query,
)

from config.config import (
    PATH_TO_CREDENTIALS,
    SERVER_TO_USE
)

from flask_caching import Cache
cache = Cache()




def fetch_past_data_from_db(
        path_to_credentials: str = PATH_TO_CREDENTIALS,
        server_to_use: str = SERVER_TO_USE
) -> pd.DataFrame:

    # connect to database
    conn_params = get_params_from_json_file(path_to_credentials, server_to_use)
    conn = psycopg2.connect(**conn_params)


    # Fetch data
    # Note: One order can have multiple payments,
    # e.g. when vouchers are involved. For convenience, to not get
    # duplicate orders in those cases, we just sum the payment_values
    # and include the first payment for information
    query = """
        WITH payments AS (
          SELECT
            payments.order_id,
            payments.payment_sequential,
            payments.payment_type,
            payments.payment_installments,
            SUM (payments.payment_value) OVER (PARTITION BY payments.order_id) AS payment_value
          FROM visible.order_payments payments
        )
        SELECT
          orders.order_id,
          orders.customer_id,
          orders.order_status,
          orders.order_purchase_timestamp,
          orders.order_approved_at,
          orders.order_delivered_carrier_date,
          orders.order_delivered_customer_date,
          orders.order_estimated_delivery_date,
          payments.payment_sequential,
          payments.payment_type,
          payments.payment_installments,
          payments.payment_value,
          customers.customer_unique_id,
          customers.customer_surname AS surname,
          customers.customer_lastname AS lastname,
          geo.geolocation_zip_code_prefix AS zip_code_prefix,
          geo.geolocation_lat AS lat,
          geo.geolocation_lng AS lng,
          geo.geolocation_city AS city,
          geo.geolocation_state AS state
        FROM visible.orders
        LEFT JOIN payments
          USING (order_id)
        LEFT JOIN visible.customers
          USING (customer_id)
        LEFT JOIN visible.geolocation geo
          ON customers.customer_zip_code_prefix = geo.geolocation_zip_code_prefix
        WHERE TRUE
          AND order_purchase_timestamp BETWEEN date_trunc('day', now()) - INTERVAL '18 month'
                                               AND date_trunc('day', now()) - INTERVAL '1 second'
          AND payments.payment_sequential = 1
        ORDER BY order_purchase_timestamp DESC
    """
    df_past = run_query(conn=conn, query=query, return_df=True)

    # add columns
    df_past['date'] = df_past['order_purchase_timestamp'].dt.strftime('%Y-%m-%d')
    df_past['dow'] = df_past['order_purchase_timestamp'].dt.strftime('%A')
    df_past['dow_short'] = df_past['order_purchase_timestamp'].dt.strftime('%a')
    df_past['time'] = df_past['order_purchase_timestamp'].dt.strftime('%H:%M:%S')

    # create an id column and set it as index,
    # so dash data table will use it as row_id
    # https://dash.plotly.com/datatable/interactivity
    df_past['id'] = df_past['order_id']
    df_past.set_index('id', inplace=True, drop=False)

    return df_past





def fetch_past_data_from_db_as_json() -> Optional[str]:
    df = fetch_past_data_from_db(PATH_TO_CREDENTIALS, SERVER_TO_USE)
    return df.to_json(date_format='iso', orient='split')





def fetch_item_past_data_from_db(
        path_to_credentials: str = PATH_TO_CREDENTIALS,
        server_to_use: str = SERVER_TO_USE
) -> pd.DataFrame:

    # connect to database
    conn_params = get_params_from_json_file(path_to_credentials, server_to_use)
    conn = psycopg2.connect(**conn_params)

    # Fetch items
    query_items = """
            SELECT
              items.order_id,
              items.product_id,
              translations.product_category_name_english AS category,
              items.item_qty,
              items.price
            FROM visible.order_items items
            LEFT JOIN visible.orders
              USING (order_id)
            LEFT JOIN visible.products
              USING (product_id)
            JOIN visible.prod_cat_translations translations
              USING (product_category_name)
            WHERE order_purchase_timestamp > date_trunc('day', now()) - INTERVAL '18 month'
            ORDER BY order_purchase_timestamp DESC
    """
    df_items_past = run_query(conn=conn, query=query_items, return_df=True)

    return df_items_past





def fetch_item_past_data_from_db_as_json() -> Optional[str]:
    df = fetch_item_past_data_from_db(PATH_TO_CREDENTIALS, SERVER_TO_USE)
    return df.to_json(date_format='iso', orient='split')





def fetch_today_data_from_db(
        path_to_credentials: str = PATH_TO_CREDENTIALS,
        server_to_use: str = SERVER_TO_USE
) -> pd.DataFrame:

    # connect to database
    conn_params = get_params_from_json_file(path_to_credentials, server_to_use)
    conn = psycopg2.connect(**conn_params)


    # Fetch data
    query_today = """
        WITH payments AS (
          SELECT
            payments.order_id,
            payments.payment_sequential,
            payments.payment_type,
            payments.payment_installments,
            SUM (payments.payment_value) OVER (PARTITION BY payments.order_id) AS payment_value
          FROM visible.order_payments payments
        )
        SELECT
          orders.order_id,
          orders.customer_id,
          orders.order_status,
          orders.order_purchase_timestamp,
          orders.order_approved_at,
          orders.order_delivered_carrier_date,
          orders.order_delivered_customer_date,
          orders.order_estimated_delivery_date,
          payments.payment_sequential,
          payments.payment_type,
          payments.payment_installments,
          payments.payment_value,
          customers.customer_unique_id,
          customers.customer_surname AS surname,
          customers.customer_lastname AS lastname,
          geo.geolocation_zip_code_prefix AS zip_code_prefix,
          geo.geolocation_lat AS lat,
          geo.geolocation_lng AS lng,
          geo.geolocation_city AS city,
          geo.geolocation_state AS state
        FROM visible.orders
        LEFT JOIN payments
          USING (order_id)
        LEFT JOIN visible.customers
          USING (customer_id)
        LEFT JOIN visible.geolocation geo
          ON customers.customer_zip_code_prefix = geo.geolocation_zip_code_prefix
        WHERE TRUE
          AND order_purchase_timestamp >= date_trunc('day', now())
          AND payments.payment_sequential = 1
        ORDER BY order_purchase_timestamp DESC
    """
    df_today = run_query(conn=conn, query=query_today, return_df=True)


    # add columns
    df_today['date'] = df_today['order_purchase_timestamp'].dt.strftime('%Y-%m-%d')
    df_today['dow'] = df_today['order_purchase_timestamp'].dt.strftime('%A')
    df_today['dow_short'] = df_today['order_purchase_timestamp'].dt.strftime('%a')
    df_today['time'] = df_today['order_purchase_timestamp'].dt.strftime('%H:%M:%S')

    df_today['id'] = df_today['order_id']
    df_today.set_index('id', inplace=True, drop=False)

    return df_today





def fetch_today_data_from_db_as_json() -> Optional[str]:
    df = fetch_today_data_from_db(PATH_TO_CREDENTIALS, SERVER_TO_USE)
    return df.to_json(date_format='iso', orient='split')





def fetch_item_today_data_from_db(
        path_to_credentials: str = PATH_TO_CREDENTIALS,
        server_to_use: str = SERVER_TO_USE
) -> pd.DataFrame:

    # connect to database
    conn_params = get_params_from_json_file(path_to_credentials, server_to_use)
    conn = psycopg2.connect(**conn_params)

    # fetch items
    query_items_today = """
            SELECT
              items.order_id,
              items.product_id,
              translations.product_category_name_english AS category,
              items.item_qty,
              items.price
            FROM visible.order_items items
            LEFT JOIN visible.orders
              USING (order_id)
            LEFT JOIN visible.products
              USING (product_id)
            JOIN visible.prod_cat_translations translations
              USING (product_category_name)
            WHERE order_purchase_timestamp >= date_trunc('day', now())
            ORDER BY order_purchase_timestamp DESC
    """
    df_items_today = run_query(conn=conn, query=query_items_today, return_df=True)

    return df_items_today





def fetch_item_today_data_from_db_as_json() -> Optional[str]:
    df = fetch_item_today_data_from_db(PATH_TO_CREDENTIALS, SERVER_TO_USE)
    return df.to_json(date_format='iso', orient='split')





# Cache past data
# https://flask-caching.readthedocs.io/en/latest/index.html#caching-other-functions
# @cache.cached(key_prefix='past_data')
# NOTE: need the way over json here, otherwise not working
@cache.memoize(timeout=None)
def load_past_data_from_global_cache():
    return pd.read_json(fetch_past_data_from_db_as_json(), orient='split')





@cache.memoize(timeout=None)
def load_item_past_data_from_global_cache():
    return pd.read_json(fetch_item_past_data_from_db_as_json(), orient='split')





def clear_data_cache():
    cache.delete_memoized(load_past_data_from_global_cache)
    cache.delete_memoized(load_item_past_data_from_global_cache)





# unnecessary to use json here, but ensures that past
# and today have same col formats in the end
def load_today_data():
    return pd.read_json(fetch_today_data_from_db_as_json(), orient='split')





def load_item_today_data():
    return pd.read_json(fetch_item_today_data_from_db_as_json(), orient='split')




# Use explicit caching (key-value) while using session_id within key
def save_cache_update_interval_config(session_id, update_interval, display_update_interval):
    """
    Cache update_interval and display_update_interval. Returns nothing.
    """
    cache.cache.set(f'{session_id}_update_interval', update_interval)
    cache.cache.set(f'{session_id}_display_update_interval', display_update_interval)
    debug_print(
        f'save_cache_update_interval_config: {session_id}, {update_interval}, {display_update_interval}'
    )
    pass





def append_todays_data_to_past_data(
        df_past: pd.DataFrame,
        df_today: pd.DataFrame
) -> pd.DataFrame:
    return pd.concat([df_past, df_today], axis=0)





# Aggregation tables
def aggregate_data_by_date(df: pd.DataFrame) -> pd.DataFrame:
    return df\
        .groupby(
            [
                'date',
                'dow',
                'dow_short'
            ]
        )\
        .agg(
            sales=('order_id', 'size'),
            revenue=('payment_value', 'sum')
        )\
        .reset_index()





# Aggregate by city
def aggregate_data_by_city(df: pd.DataFrame) -> pd.DataFrame:
    agg_by_city = df.copy()
    agg_by_city['lng'] = agg_by_city['lng'].astype('float')
    agg_by_city['lat'] = agg_by_city['lat'].astype('float')
    agg_by_city['order_id'] = agg_by_city['order_id'].astype('string')

    # be explicit with colnames since may result
    # in level_0, level_1, ... when df is empty
    return agg_by_city\
        .groupby(
            [
                'city',
                'state',
                'date'
            ]
        )\
        .agg(
            sales=('order_id', 'size'),
            revenue=('payment_value', 'sum'),
            lat=('lat', 'mean'),
            lng=('lng', 'mean')
        )\
        .reset_index()\
        .set_axis(
            [
                'city',
                'state',
                'date',
                'sales',
                'revenue',
                'lat',
                'lng'
            ],
            axis='columns'
        )





# single_customer_table
def aggregate_items_by_order_id(df: pd.DataFrame) -> pd.DataFrame:
    return df\
        .groupby(
            [
                'order_id'
            ]
        )\
        .agg(
            product_id=('product_id', 'first'),
            category=('category', 'first'),
            total_items=('product_id', 'size'),
            total_qty=('item_qty', 'sum'),
            #order_value=('price', 'sum')
        )\
        .reset_index()





# new customer ratio
def total_orders_by_customer(df: pd.DataFrame) -> pd.DataFrame:
    return df\
        .groupby(
            [
                'customer_id'
            ]
        )\
        .agg(
            total_orders=('customer_id', 'size')
        )\
        .reset_index()

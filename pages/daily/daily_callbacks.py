from datetime import datetime
from utils.functions import debug_print

import dash
from dash import (
    dash_table,
    callback_context,
)
from dash.dependencies import Output, Input, State
from dash.exceptions import PreventUpdate

from pages.daily.daily_data import (
    load_past_data_from_global_cache,
    load_item_past_data_from_global_cache,
    clear_data_cache,
    fetch_today_data_from_db_as_json,
    fetch_item_today_data_from_db_as_json,
    save_cache_update_interval_config,
    cache
)

from pages.daily.daily_graphs import (
    generate_graph_and_dropdown_last_updated,
    generate_graph_sales_sv,
    generate_graph_revenue_sv,
    generate_graph_new_customer_ratio_sv,
    generate_graph_sales_week_bar,
    generate_graph_sales_18m_line,
    generate_graph_orders_map,
    cls_table_orders_container,
    cls_table_order_details_container,
    cls_table_customer_details_container,
)

from utils.pgfunctions import (
    get_last_updated_value
)

from sql.update_visible import update_visible
from app import app
from flask import session


# get tempalte formats for data table
FT_money = dash_table.FormatTemplate.money(2)




# @app.callback(
#     Output('show_click_output', 'children'),
#     Input(component_id='map_orders', component_property='clickData'),
#     # Input('table_orders', 'active_cell'),
#     prevent_initial_call=True
# )
# def show_clickData_output(clickData):
#     return json.dumps(clickData, indent=2)





# load and cache past data
@app.callback(
    Output('trigger_today_data_fetching', 'data'),
    Input('trigger_past_data_fetching', 'data'),
)
def load_and_cache_past_df(_):
    # possible to run both in parallel?
    load_past_data_from_global_cache()
    load_item_past_data_from_global_cache()
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f")





# clear cache (open modal first)
@app.callback(
    Output('trigger_modal_to_confirm_refresh_cache_to_close', 'data'),
    Input('btn_refresh_all', 'n_clicks'),
    prevent_initial_call=True
)
def clear_cache_then_reload_and_cache_past_df(n_clicks):
    if n_clicks is not None:
        return datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f")




# Confirm to clear cache
@app.callback(
    Output('trigger_past_data_fetching', 'data'),
    Output('trigger_modal_to_confirm_refresh_cache_to_close2', 'data'),
    Input('btn_confirm_refresh_all', 'n_clicks'),
    prevent_initial_call=True
)
def confirm_to_clear_cache(n_clicks):
    if n_clicks is not None:
        clear_data_cache()
        _timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S:%f")
        return _timestamp, _timestamp





# Trigger open/close modal (for clear cache)
# Note: need n_clicks here because prevent_initial_call
# does not work, probably because layout is created dynamically
# and also generate_modal_to_confirm_cache_refresh called as function:
# https://community.plotly.com/t/prevent-initial-callback-ineffective/54663/4
@app.callback(
    Output('modal_confirm_refresh_all', 'is_open'),
    Input('trigger_modal_to_confirm_refresh_cache_to_close', 'data'),
    Input('trigger_modal_to_confirm_refresh_cache_to_close2', 'data'),
    State('modal_confirm_refresh_all', 'is_open'),
    State('btn_refresh_all', 'n_clicks'),
    prevent_initial_call=True
)
def close_modal_confirm_to_clear_cache(_a, _b, is_open, n_clicks):
    if n_clicks:
        return not is_open





# fetch todays data and store locally
@app.callback(
    Output('local_store_today_data', 'data'),
    Output('local_store_item_today_data', 'data'),
    Input('trigger_today_data_fetching', 'data'),
    Input('trigger_today_data_fetching2', 'data'),
    prevent_initial_call=True
)
def load_today_data_and_store_locally(_a, _b):
    return fetch_today_data_from_db_as_json(), fetch_item_today_data_from_db_as_json()





# Refresh today data (fetch and store locally)
@app.callback(
    Output('trigger_today_data_fetching2', 'data'),
    Output('last_updated_sv', 'children'),
    Input('refresh_now_update_visible', 'n_clicks'),
    Input('interval_trigger_today_data_refresh', 'n_intervals'),
    prevent_initial_call=True
)
def refresh_update_visible_button_or_interval(n_clicks, n_intervals):
    if n_clicks is not None or n_intervals is not None:
        update_visible() # this would actually happen independently on server
        trigger = datetime.now().strftime("%Y-%m-%d %H:%M:%S:f")
        last_refreshed_timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        newest_datapoint_in_db = get_last_updated_value()
        last_updated_sv = '**Newest datapoint**: ' + newest_datapoint_in_db + ' | ' + \
            '**Last refresh**: ' +  last_refreshed_timestamp
        return trigger, last_updated_sv
    else:
        raise PreventUpdate




# Set update interval (for today data)
@app.callback(
    Output('trigger_interval_trigger_today_data_refresh', 'data'),
    Output('display_current_update_interval', 'children'),
    Input('today_refresh_interval_10s', 'n_clicks'),
    Input('today_refresh_interval_30s', 'n_clicks'),
    Input('today_refresh_interval_1m', 'n_clicks'),
    Input('today_refresh_interval_5m', 'n_clicks'),
    Input('today_refresh_interval_10m', 'n_clicks'),
    State('today_refresh_interval_custom_number', 'value'),
    State('today_refresh_interval_custom_unit', 'value'),
    Input('today_refresh_interval_custom_btn', 'n_clicks'),
    prevent_initial_call=True
)
def cb_cache_update_interval(i10s, i30s, i1m, i5m, i10m, custom_num, custom_unit, custom_btn):
    # https://dash.plotly.com/dash-html-components/button
    # https://dash.plotly.com/advanced-callbacks
    # on page reload: in case no btn has been triggered yet, do nothing
    # https://community.plotly.com/t/unwanted-callback-on-page-load/9971/9
    if not [p['prop_id'] for p in callback_context.triggered if p['value'] is not None]:
        return dash.no_update, dash.no_update
    changed_id = [p['prop_id'] for p in callback_context.triggered if p['value'] is not None][0]
    if 'today_refresh_interval_10s' in changed_id:
        interval = 1000 * 10
        display_interval = 'Update interval: 10 sec'
    elif 'today_refresh_interval_30s' in changed_id:
        interval = 1000 * 30
        display_interval = 'Update interval: 30 sec'
    elif 'today_refresh_interval_1m' in changed_id:
        interval = 1000 * 60
        display_interval = 'Update interval: 1 min'
    elif 'today_refresh_interval_5m' in changed_id:
        interval = 1000 * 60 * 5
        display_interval = 'Update interval: 5 min'
    elif 'today_refresh_interval_10m' in changed_id:
        interval = 1000 * 60 * 10
        display_interval = 'Update interval: 10 min'
    elif 'today_refresh_interval_custom_btn' in changed_id:
        if custom_unit == 'sec':
            unit = 1
        elif custom_unit == 'min':
            unit = 60
        elif custom_unit == 'h':
            unit = 60 * 60
        interval = 1000 * int(custom_num) * unit
        display_interval = 'Update interval: ' + str(custom_num) + ' ' + custom_unit
    else:
        interval = 1000 * 60
        display_interval = 'Update interval: 1 min'

    if 'session_id' in session:
        session_id = session.get('session_id')
    else:
        session_id = ''

    save_cache_update_interval_config(session_id, interval, display_interval)
    trigger = interval # only trigger when interval has changed

    return trigger, display_interval






# Update the dcc.Interval
@app.callback(
    Output('interval_trigger_today_data_refresh', 'interval'),
    Input('trigger_interval_trigger_today_data_refresh', 'data'),
    Input('trigger_interval_trigger_today_data_refresh2', 'data'),
)
def update_interval_trigger_today_data_refresh(_a, _b):
    if 'session_id' in session:
        session_id = session.get('session_id')
    else:
        session_id = ''

    update_interval = cache.cache.get(f'{session_id}_update_interval')
    if update_interval is None:
        update_interval = 60000

    debug_print(f'callback: update_interval_trigger_today_data_refresh: {session_id}, {update_interval}')

    return update_interval





# dropdown and last updated
@app.callback(
    Output('graph_and_dropdown_last_updated', 'children'),
    Input('local_store_today_data', 'data'),
    prevent_initial_call=True
)
def make_last_updated_sv(_):
    if 'session_id' in session:
        session_id = session.get('session_id')
    else:
        session_id = ''
    return generate_graph_and_dropdown_last_updated(session_id=session_id)





# graph: sales sv
@app.callback(
    Output('sales_sv', 'children'),
    Input('local_store_today_data', 'data'),
    prevent_initial_call=True
)
def make_sales_sv(input_from_local_store_today_data):
    return generate_graph_sales_sv(input_from_local_store_today_data)





# graph: revenue sv
@app.callback(
    Output('revenue_sv', 'children'),
    Input('local_store_today_data', 'data'),
    prevent_initial_call=True
)
def make_revenue_sv(input_from_local_store_today_data):
    return generate_graph_revenue_sv(input_from_local_store_today_data)





# graph: new customer ratio sv
@app.callback(
    Output('new_customer_ratio_sv', 'children'),
    Input('local_store_today_data', 'data'),
    prevent_initial_call=True
)
def make_new_customer_ratio_sv(input_from_local_store_today_data):
    return generate_graph_new_customer_ratio_sv(input_from_local_store_today_data)





# show graph config
@app.callback(
    Output('collapse_graph_control', 'is_open'),
    Input('btn_show_graph_config', 'n_clicks'),
    State('collapse_graph_control', 'is_open'),
    prevent_initial_call=True
)
def show_graph_config_collapse(n_clicks, is_open):
    if n_clicks:
        return not is_open
    else:
        return is_open






# graph: 1w bar
@app.callback(
    Output(component_id='bar_1w', component_property='figure'),
    Input(component_id='dropdown_toggle_sales_rev', component_property='value'),
    Input('local_store_today_data', 'data'),
    Input('checklist_graphs_show_only_for_selected_city', 'value'),
    Input('map_orders', 'clickData'),
    prevent_initial_call=True
)
def make_bar_1w_chart(
        dropdown_toggle_sales_rev,
        input_from_local_store_today_data,
        input_from_checklist_graphs_show_only_for_selected_city,
        map_click_data
):
    if input_from_checklist_graphs_show_only_for_selected_city:
        if map_click_data is None:
            city_filter = 'Sao Paulo'
        else:
            city_filter = map_click_data['points'][0]['hovertext']
    else:
        city_filter = None

    if dropdown_toggle_sales_rev == 'sales':
        y = 'sales'
    else:
        y = 'revenue'

    return generate_graph_sales_week_bar(
        input_from_local_store_today_data,
        x='dow_short',
        y=y,
        city_filter=city_filter
    )





# graph: 18m line
@app.callback(
    Output('line_18m', 'figure'),
    Input('dropdown_toggle_sales_rev', 'value'),
    Input('local_store_today_data', 'data'),
    Input('checklist_graphs_show_only_for_selected_city', 'value'),
    Input('map_orders', 'clickData'),
    prevent_initial_call=True
)
def make_line_18m_chart(
        dropdown_toggle_sales_rev,
        input_from_local_store_today_data,
        input_from_checklist_graphs_show_only_for_selected_city,
        map_click_data
):
    if input_from_checklist_graphs_show_only_for_selected_city:
        if map_click_data is None:
            city_filter = 'Sao Paulo'
        else:
            city_filter = map_click_data['points'][0]['hovertext']
    else:
        city_filter = None

    if dropdown_toggle_sales_rev == 'sales':
        y = 'sales'
    else:
        y = 'revenue'

    return generate_graph_sales_18m_line(
        input_from_local_store_today_data,
        x='date',
        y=y,
        city_filter=city_filter
    )




# graph: map
@app.callback(
    Output('map_orders', 'figure'),
    Input('local_store_today_data', 'data'),
    prevent_initial_call=True
)
def make_orders_map(input_from_local_store_today_data):
    return generate_graph_orders_map(input_from_local_store_today_data)









# Table: init tables at startup
@app.callback(
    Output('table_orders_div_container', 'children'),
    Input('hidden_div_for_empty_input', 'children'),
)
def init_orders_table_container_at_startup(_):
    table_orders_container = cls_table_orders_container()
    return table_orders_container.return_table_orders_container()





# Table: Update Oders table
@app.callback(
    Output('table_orders_card_city', 'children'),
    Output('table_orders_card_sales', 'children'),
    Output('table_orders_card_revenue', 'children'),
    Output('table_orders', 'data'),
    Output('table_orders', 'columns'),
    Input('local_store_today_data', 'data'),
    Input('map_orders', 'clickData'),
    prevent_initial_call=True
)
def update_orders_table(input_from_local_store_today_data, map_click_data):
    if map_click_data is None:
        city_filter = 'Sao Paulo'
    else:
        city_filter = str(map_click_data['points'][0]['hovertext'])

    table_orders_container = cls_table_orders_container()
    table_orders_container.update_data(input_from_local_store_today_data, city_filter)
    return table_orders_container.return_elemets_for_callback()





# Table: Update details table: customer or order
@app.callback(
    Output('table_customer_or_order_header', 'children'),
    Output('table_customer_or_order_div_container', 'children'),
    Input('table_orders', 'active_cell'),
    State('local_store_today_data', 'data'),
    State('local_store_item_today_data', 'data'),
    prevent_initial_call=True
)
def make_customer_or_order_summary_table(
        active_cell_in_table_orders,
        input_from_local_store_today_data,
        input_from_local_store_item_today_data
):
    if active_cell_in_table_orders is not None:
        col_id = active_cell_in_table_orders['column_id']
        selected_order_id = active_cell_in_table_orders['row_id']

        # single order table
        if col_id == 'order_id':
            table_order_details_container = cls_table_order_details_container()
            table_order_details_container.update_data(
                input_from_local_store_today_data,
                input_from_local_store_item_today_data,
                order_id = selected_order_id
            )
            return table_order_details_container.return_table_order_details_container()

        # single customer table
        elif col_id in (['customer_id', 'surname', 'lastname']):
            table_customer_details_container = cls_table_customer_details_container()
            table_customer_details_container.update_data(
                input_from_local_store_today_data,
                input_from_local_store_item_today_data,
                order_id = selected_order_id
            )
            return table_customer_details_container.return_table_customer_details_container()

        else:
            raise PreventUpdate

    else:
        return (None, None)
